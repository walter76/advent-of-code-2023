use aoc_core::text_map::TextMap;

const EXAMPLE_PLATFORM: &str = r"O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();

    let platform = TextMap::from_str(EXAMPLE_PLATFORM);
    // let platform = TextMap::from_str(&input);

    // print!("{}", platform);
    // println!();

    let tilted_platform = tilt_platform_north(&platform);

    // print!("{}", tilted_platform);
    println!("{}", calculate_total_load(&tilted_platform));

    println!();
    println!();
    
    println!("Original Platform");
    print!("{}", platform);
    println!();

    let tilted_platform = tilt_platform_north(&platform);
    println!("Tilted North");
    print!("{}", tilted_platform);
    println!();

    let tilted_platform = tilt_platform_east(&tilted_platform);
    println!("Tilted East");
    print!("{}", tilted_platform);
    println!();
}

fn calculate_total_load(platform: &TextMap) -> usize {
    let platform_height = platform.height();

    let mut total_rock_load = 0;

    for x in 0 .. platform.width() {
        let rock_load: Vec<usize> = platform.get_column(x).iter()
            .enumerate().filter(|(_, c)| **c == 'O').map(|(i, _)| platform_height - i).collect();

        total_rock_load += rock_load.iter().sum::<usize>();
    }

    total_rock_load
}

fn tilt_platform_north(platform: &TextMap) -> TextMap {
    let mut tilted_platform = platform.clone();

    // remove all rocks from the platform
    tilted_platform.replace_all('O', '.');

    for x in 0 .. platform.width() {
        let rock_positions: Vec<usize> = platform.get_column(x).iter()
            .enumerate().filter(|(_, c)| **c == 'O').map(|(i, _)| i).collect();

        for y_rock in rock_positions.iter() {
            match search_next_free_position_north(&tilted_platform.get_column(x), *y_rock) {
                Some(free_position) => tilted_platform.set_char_at(x, free_position, 'O'),
                None => ()
            }
        }
    }

    tilted_platform
}

fn search_next_free_position_north(platform_column: &[char], start_y: usize) -> Option<usize> {
    for y in (0 .. start_y + 1).rev() {
        if platform_column[y] == '#' || platform_column[y] == 'O' {
            return Some(y + 1);
        }
    }

    if platform_column[0] == '#' || platform_column[0] == 'O' {
        Some(1)
    } else if platform_column[0] == '.' {
        Some(0)
    } else {
        None
    }
}

fn tilt_platform_east(platform: &TextMap) -> TextMap {
    let mut tilted_platform = platform.clone();

    // remove all rocks from the platform
    tilted_platform.replace_all('O', '.');

    for y in 0 .. platform.height() {
        let rock_positions: Vec<usize> = platform.get_row(y).iter()
            .enumerate().filter(|(_, c)| **c == 'O').map(|(i, _)| i).collect();

        for x_rock in rock_positions.iter() {
            match search_next_free_position_east(&tilted_platform.get_row(y), *x_rock) {
                Some(free_position) => tilted_platform.set_char_at(free_position, y, 'O'),
                None => ()
            }
        }
    }

    tilted_platform
}

fn search_next_free_position_east(platform_row: &[char], start_x: usize) -> Option<usize> {
    for x in start_x .. platform_row.len() {
        if platform_row[x] == '#' || platform_row[x] == 'O' {
            return Some(x - 1);
        }
    }

    if platform_row[platform_row.len() - 1] == '#' || platform_row[platform_row.len() - 1] == 'O' {
        Some(platform_row.len() - 2)
    } else if platform_row[platform_row.len() - 1] == '.' {
        Some(platform_row.len() - 1)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use aoc_core::text_map::TextMap;

    use crate::{EXAMPLE_PLATFORM, tilt_platform_north};

    const EXAMPLE_PLATFORM_TILTED_NORTH: &str = r"OOOO.#.O..
OO..#....#
OO..O##..O
O..#.OO...
........#.
..#....#.#
..O..#.O.O
..O.......
#....###..
#....#....";

    #[test]
    fn tilt_platform_north_should_roll_all_rocks_north() {
        let example_platform = TextMap::from_str(EXAMPLE_PLATFORM);
        let tilted_platform = TextMap::from_str(EXAMPLE_PLATFORM_TILTED_NORTH);

        assert_eq!(tilted_platform, tilt_platform_north(&example_platform));
    }
}
