use std::{collections::HashMap, fmt::Display};

use once_cell::sync::Lazy;
use regex::Regex;

const EXAMPLE_INPUT: &str = r"px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}";

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();
    let workflow_engine = WorkflowEngine::from_str(&input);

    let parts_accepted = workflow_engine.start();

    println!();

    let sum_total: i64 = parts_accepted.iter().map(|part| part.sum()).sum();

    println!("{}", sum_total);
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Property {
    ExtremelyCoolLooking,
    Musical,
    Aerodynamic,
    Shiny
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Condition {
    LessThan(i64),
    GreaterThan(i64),
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Operation {
    Accept,
    Reject,
    Send(String),
}

// Rule `x>10:one`: If the part's `x` is more than `10`, send the part to the workflow named `one`.
const RULE_1: &str = "x>10:one";

// Rule `m<20:two`: If the part's `m` is less than `20`, send the part to the workflow named `two`.
const RULE_2: &str = "m<20:two";

// Rule `a>30:R`: If the part's `a` is more than `30`, the part is immediately __rejected__ (R).
const RULE_3: &str = "a>30:R";

// Rule `A`: The part is immediately __accepted__ (A)
const RULE_4: &str = "A";

// Rule `one`: The part is send to the workflow named `one`.
const RULE_5: &str = "one";

const PART_1: &str = "{x=787,m=2655,a=1222,s=2876}";
const PART_3: &str = "{x=2036,m=264,a=79,s=2244}";
const PART_5: &str = "{x=2127,m=1623,a=2188,s=1013}";

const WORKFLOW_1: &str = "px{a<2006:qkq,m>2090:A,rfg}";
const WORKFLOW_2: &str = "pv{a>1716:R,A}";

static RULE_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"^(\w)(>|<)(\d+):(\w+)$").unwrap()
});

#[derive(Debug, Clone, Eq, PartialEq)]
struct Rule {
    property: Option<Property>,
    condition: Option<Condition>,
    operation: Operation,
}

impl Rule {
    pub fn from_str(rule_text: &str) -> Self {
        if let Some(captures) = RULE_REGEX.captures(rule_text) {
            let mut property: Option<Property> = None;

            if let Some(property_capture) = captures.get(1) {
                match property_capture.as_str() {
                    "x" => property = Some(Property::ExtremelyCoolLooking),
                    "m" => property = Some(Property::Musical),
                    "a" => property = Some(Property::Aerodynamic),
                    "s" => property = Some(Property::Shiny),
                    _ => unimplemented!()
                }
            } else {
                unimplemented!();
            }

            let mut condition: Option<Condition> = None;

            if let Some(condition_capture) = captures.get(2) {
                let value = if let Some(value_capture) = captures.get(3) {
                    value_capture.as_str().parse::<i64>().unwrap()
                } else {
                    unimplemented!()
                };

                match condition_capture.as_str() {
                    ">" => condition = Some(Condition::GreaterThan(value)),
                    "<" => condition = Some(Condition::LessThan(value)),
                    _ => unimplemented!()
                }
            } else {
                unimplemented!();
            }

            if let Some(operation_capture) = captures.get(4) {
                let operation_text = operation_capture.as_str();

                if operation_text.len() > 1 {
                    Self {
                        property,
                        condition,
                        operation: Operation::Send(operation_text.to_string()),
                    }
                } else if operation_text.len() == 1 {
                    let operation = match operation_text.chars().nth(0) {
                        Some('A') => Operation::Accept,
                        Some('R') => Operation::Reject,
                        _ => unimplemented!(),
                    };

                    Self {
                        property,
                        condition,
                        operation,
                    }
                } else {
                    unimplemented!();
                }
            } else {
                unimplemented!();
            }
        } else {
            let operation = match rule_text {
                "A" => Operation::Accept,
                "R" => Operation::Reject,
                target_workflow => Operation::Send(target_workflow.to_string()),
            };

            Self {
                property: None,
                condition: None,
                operation,
            }
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Part {
    x: i64,
    m: i64,
    a: i64,
    s: i64,
}

impl Part {
    pub fn from_str(part_text: &str) -> Self {
        let normalized = &part_text[ 1 .. part_text.len() - 1 ];
        let mut properties = normalized.split(',');

        let x = if let Some(x_text) = properties.next() {
            x_text[2 .. ].parse::<i64>().unwrap()
        } else {
            unimplemented!()
        };

        let m = if let Some(m_text) = properties.next() {
            m_text[2 .. ].parse::<i64>().unwrap()
        } else {
            unimplemented!()
        };

        let a = if let Some(a_text) = properties.next() {
            a_text[2 .. ].parse::<i64>().unwrap()
        } else {
            unimplemented!()
        };

        let s = if let Some(s_text) = properties.next() {
            s_text[2 .. ].parse::<i64>().unwrap()
        } else {
            unimplemented!()
        };

        Self { x, m, a, s, }
    }

    pub fn sum(&self) -> i64 {
        self.x + self.m + self.a + self.s
    }

    pub fn get_property_value(&self, property: Property) -> i64 {
        match property {
            Property::ExtremelyCoolLooking => self.x,
            Property::Musical => self.m,
            Property::Aerodynamic => self.a,
            Property::Shiny => self.s,
        }
    }
}

impl Display for Part {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{x={},m={},a={},s={}}}", self.x, self.m, self.a, self.s)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
}

impl Workflow {
    pub fn from_str(workflow_text: &str) -> Self {
        let pos = workflow_text.find('{').unwrap();

        let name = workflow_text[ 0 .. pos ].to_string();

        let mut rules: Vec<Rule> = vec![];

        for rule_text in workflow_text[ pos + 1 .. workflow_text.len() - 1 ].split(',') {
            rules.push(Rule::from_str(rule_text));
        }

        Self {
            name,
            rules,
        }
    }

    pub fn execute(&self, part: &Part) -> Operation {
        for rule in self.rules.iter() {
            match rule.condition {
                Some(Condition::LessThan(v)) => {
                    if let Some(property) = rule.property.clone() {
                        if part.get_property_value(property) < v {
                            return rule.operation.clone();
                        } else {
                            continue;
                        }
                    } else {
                        unimplemented!();
                    }
                }

                Some(Condition::GreaterThan(v)) => {
                    if let Some(property) = rule.property.clone() {
                        if part.get_property_value(property) > v {
                            return rule.operation.clone();
                        } else {
                            continue;
                        }
                    } else {
                        unimplemented!();
                    }
                }

                None => {
                    return rule.operation.clone();
                }
            }
        }

        unimplemented!();
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct WorkflowEngine {
    workflows: HashMap<String, Workflow>,
    parts: Vec<Part>,
}

impl WorkflowEngine {
    pub fn from_str(input_text: &str) -> Self {
        let mut input_iter = input_text.lines();

        let mut workflows: HashMap<String, Workflow> = HashMap::new();

        while let Some(line) = input_iter.next() {
            if line.is_empty() {
                break;
            }

            let workflow = Workflow::from_str(line);
            workflows.insert(workflow.name.clone(), workflow);
        }

        let mut parts: Vec<Part> = vec![];

        while let Some(line) = input_iter.next() {
            parts.push(Part::from_str(line));
        }

        Self {
            workflows,
            parts,
        }
    }

    pub fn start(&self) -> Vec<Part> {
        let mut parts_accepted: Vec<Part> = vec![];

        for part in self.parts.iter() {
            let mut workflow_name = "in".to_string();

            print!("{}:", part);

            loop {
                print!(" -> {}", workflow_name);

                match self.execute_workflow_on_part(part, &workflow_name) {
                    Operation::Send(n) => workflow_name = n.clone(),
                    Operation::Accept => {
                        parts_accepted.push(part.clone());

                        println!(" -> A");

                        break;
                    },
                    Operation::Reject => {
                        println!(" -> R");

                        break;
                    },
                }
            }
        }

        parts_accepted
    }

    fn execute_workflow_on_part(&self, part: &Part, workflow_name: &str) -> Operation {
        let workflow = self.workflows.get(workflow_name).unwrap();
        workflow.execute(part)
    }
}

#[cfg(test)]
mod tests {
    use crate::{RULE_4, Rule, Operation, Property, RULE_1, Condition, RULE_2, RULE_3, Part, PART_1, PART_3, PART_5, RULE_5, Workflow, WORKFLOW_1, WORKFLOW_2};

    #[test]
    fn rule_from_str_should_parse_rule_4_as_accepted() {
        let rule = Rule::from_str(RULE_4);

        assert_eq!(
            Rule {
                property: None,
                condition: None,
                operation: Operation::Accept
            },
            rule
        );
    }

    #[test]
    fn rule_from_str_should_parse_rule_as_rejected() {
        let rule = Rule::from_str("R");

        assert_eq!(
            Rule {
                property: None,
                condition: None,
                operation: Operation::Reject
            },
            rule
        );
    }

    #[test]
    fn rule_from_str_should_parse_rule_5_as_send() {
        let rule = Rule::from_str(RULE_5);

        assert_eq!(
            Rule {
                property: None,
                condition: None,
                operation: Operation::Send("one".to_string())
            },
            rule
        );
    }

    #[test]
    fn rule_from_str_should_parse_rule_1() {
        let rule = Rule::from_str(RULE_1);

        assert_eq!(
            Rule {
                property: Some(Property::ExtremelyCoolLooking),
                condition: Some(Condition::GreaterThan(10)),
                operation: Operation::Send("one".to_string()),
            },
            rule
        );
    }

    #[test]
    fn rule_from_str_should_parse_rule_2() {
        let rule = Rule::from_str(RULE_2);

        assert_eq!(
            Rule {
                property: Some(Property::Musical),
                condition: Some(Condition::LessThan(20)),
                operation: Operation::Send("two".to_string()),
            },
            rule
        );
    }

    #[test]
    fn rule_from_str_should_parse_rule_3() {
        let rule = Rule::from_str(RULE_3);

        assert_eq!(
            Rule {
                property: Some(Property::Aerodynamic),
                condition: Some(Condition::GreaterThan(30)),
                operation: Operation::Reject,
            },
            rule
        );
    }

    #[test]
    fn part_from_str_should_parse_part_1() {
        let part = Part::from_str(PART_1);

        assert_eq!(
            Part {
                x: 787,
                m: 2655,
                a: 1222,
                s: 2876,
            },
            part
        );
    }

    #[test]
    fn part_sum_should_be_7540_for_part_1() {
        let part = Part::from_str(PART_1);

        assert_eq!(7540, part.sum());
    }

    #[test]
    fn part_sum_should_be_4623_for_part_3() {
        let part = Part::from_str(PART_3);

        assert_eq!(4623, part.sum());
    }

    #[test]
    fn part_sum_should_be_6951_for_part_5() {
        let part = Part::from_str(PART_5);

        assert_eq!(6951, part.sum());
    }

    #[test]
    fn workflow_from_str_should_parse_workflow_1() {
        let workflow = Workflow::from_str(WORKFLOW_1);

        assert_eq!(
            Workflow {
                name: "px".to_string(),
                rules: vec![
                    Rule {
                        property: Some(Property::Aerodynamic),
                        condition: Some(Condition::LessThan(2006)),
                        operation: Operation::Send("qkq".to_string()),
                    },
                    Rule {
                        property: Some(Property::Musical),
                        condition: Some(Condition::GreaterThan(2090)),
                        operation: Operation::Accept,
                    },
                    Rule {
                        property: None,
                        condition: None,
                        operation: Operation::Send("rfg".to_string()),
                    },
                ]
            },
            workflow
        );
    }

    #[test]
    fn workflow_execute_workflow_2_on_part_1_should_accept() {
        let workflow = Workflow::from_str(WORKFLOW_2);
        let part = Part::from_str(PART_1);

        assert_eq!(Operation::Accept, workflow.execute(&part));
    }

    #[test]
    fn workflow_execute_workflow_1_on_part_1_should_send() {
        let workflow = Workflow::from_str(WORKFLOW_1);
        let part = Part::from_str(PART_1);

        assert_eq!(Operation::Send("qkq".to_string()), workflow.execute(&part));
    }
}

