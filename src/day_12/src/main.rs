use std::collections::VecDeque;

const CONDITION_RECORD_1: &str = "???.### 1,1,3";
const CONDITION_RECORD_2: &str = ".??..??...?##. 1,1,3";

const CONDITION_RECORDS: &str = r"???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();

    let mut total = 0;

    for line in input.lines() {
        let condition_record = ConditionRecord::from_str(line);
        let possible_arrangements = condition_record.possible_arrangements();

        total += possible_arrangements;

        println!("{}: {} ({})", condition_record.damaged_arrangement, total, possible_arrangements);
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct ConditionRecord {
    damaged_arrangement: String,
    groups: Vec<i32>,
}

impl ConditionRecord {
    pub fn from_str(text: &str) -> Self {
        let parts: Vec<_> = text.split_whitespace().collect();

        let damaged_arrangement = parts[0].to_string();

        let groups: Vec<i32> = parts[1].split(',').map(|g| g.parse::<i32>().unwrap()).collect();

        Self {
            damaged_arrangement,
            groups,
        }
    }

    pub fn possible_arrangements(&self) -> i32 {
        let question_marks_positions = self.question_marks_positions();
        let no_of_replacements = question_marks_positions.len();

        let permutations = self.generate_permutations(no_of_replacements);
        let mut no_of_arrangements = 0;

        for perm in permutations.iter() {
            let mut s = self.damaged_arrangement.to_string();

            for (index, c_to_replace) in perm.chars().enumerate() {
                let pos = question_marks_positions[index];

                s = s.chars().enumerate().map(|(i,c)| if i == pos { c_to_replace } else { c }).collect::<String>();
            }

            // let's count groups and check if the groups match the expectations
            let groups: Vec<i32> = s.split('.').map(|g| g.len() as i32).filter(|l| *l > 0).collect();

            if groups == self.groups {
                no_of_arrangements += 1;
            }
        }

        no_of_arrangements
    }

    fn question_marks_positions(&self) -> Vec<usize> {
        self.damaged_arrangement.chars().enumerate()
            .filter(|(_, c)| *c == '?')
            .map(|(position, _)| position)
            .collect()
    }

    fn generate_permutations(&self, no_of_replacements: usize) -> Vec<String> {
        // We create all posible permutations for '#' and '.' for the length of all question marks
        // occurences, because we do want to replace all question marks with all possible
        // permutations.
        //
        // We further could imagine that generating the permutations is like generating binary
        // numbers, while `#` is 1 and `.` is 0.
        //
        // https://www.codingninjas.com/studio/library/generate-binary-numbers-from-1-to-n-using-queue-in-java

        let mut permutations: Vec<String> = vec![];

        // the first permutation is all `.`
        permutations.push(".".repeat(no_of_replacements));

        let mut generator: VecDeque<String> = VecDeque::new();

        generator.push_front("#".to_string());

        // in principle we could say that we have to create 2 ^ no_of_replacements permutations
        let no_of_permutations = 2_u32.pow(no_of_replacements as u32);

        for _ in 0 .. no_of_permutations - 1 {
            let generated = generator.pop_front().unwrap();

            permutations.push(generated.clone());

            generator.push_back(format!("{}.", generated));
            generator.push_back(format!("{}#", generated));
        }

        // lets fill the missing '.'
        for perm in permutations.iter_mut() {
            if perm.len() < no_of_replacements {
                let a = ".".repeat(no_of_replacements - perm.len());
                perm.insert_str(0, &a);
            }
        }

        permutations
    }
}

#[cfg(test)]
mod tests {
    use crate::{ConditionRecord, CONDITION_RECORD_1};

    #[test]
    fn from_str_should_parse_text() {
        let condition_record = ConditionRecord::from_str(CONDITION_RECORD_1);

        assert_eq!("???.###".to_string(), condition_record.damaged_arrangement);
        assert_eq!(vec![ 1, 1, 3 ], condition_record.groups);
    }

}
