use aoc_core::text_map::TextMap;

const EXAMPLE_PATTERN_1: &str = r"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.";

const EXAMPLE_PATTERN_2: &str = r"#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

// # Algorithm
//
// ## Assumptions
//
// - the symetry axis can be anywhere from second row / column to the n - 1 row / column
// - to find the symetry axis, I can just look for two rows / columns that are equal
// 
// ## Columns
//
// - start with the first column and iterate till width - 1
// - check if the current column and the next columen are equal
// - if they are equal, we have found the symetry axis
// - if we do not find any equal columns, we check the rows
//
// ## Rows
//
// - start with the first row and iterate till height - 1
// - check if the current row and the next row are equal
// - if they are equal, we have found the symetry axis
//
// ## Remarks
//
// - in case, the result is not correct, it could be that there are multiple duplicate rows/cols
//   we then, would have to change algorithm, that as soon as we found the symetry axis, we check
//   how many duplicated rows/cols we have

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();
    let patterns = extract_patterns(&input);

    // let patterns = vec![
    //    TextMap::from_str(EXAMPLE_PATTERN_1), TextMap::from_str(EXAMPLE_PATTERN_2)];

    let mut total: usize = 0;

    for (index, pattern) in patterns.iter().enumerate() {
        println!("Checking pattern: {}", index);

        let value_of_pattern = check_pattern(&pattern);

        println!("{}", value_of_pattern);

        if value_of_pattern == 0 {
            println!();
            println!("{}", pattern);
            println!();
        }

        println!();

        total += value_of_pattern;
    }

    println!("Total: {}", total);
}

fn extract_patterns(input: &str) -> Vec<TextMap> {
    let mut patterns: Vec<TextMap> = vec![];
    let mut line_iterator = input.lines();
    let mut line_chunk = String::new();

    while let Some(line) = line_iterator.next() {
        if line.is_empty() {
            let pattern = TextMap::from_str(&line_chunk);

            patterns.push(pattern);

            line_chunk.clear();
        } else {
            line_chunk.push_str(&format!("{}\n", line));
        }
    }

    patterns.push(TextMap::from_str(&line_chunk));

    patterns
}

#[derive(Debug, Eq, PartialEq, Clone)]
enum SymmetryAxis {
    Vertical(usize),
    Horizontal(usize),
}

fn check_pattern(pattern: &TextMap) -> usize {
    let (vertical, horizontal) = has_symmetry_axis(pattern);

    vertical + horizontal * 100
}

fn has_symmetry_axis(pattern: &TextMap) -> (usize, usize) {
    let mut vertical = 0;

    for x_axis in get_cols_axis(&pattern).iter() {
        println!("Possible vertical symmetry axis at: {}", x_axis);

        let mut j = x_axis + 1;
        let mut is_symmetric = true;

        for i in (0 .. x_axis + 1).rev() {
            // it could happen, that we go beyond the right border
            if j >= pattern.width() {
                break;
            }

            let col_i = pattern.get_column(i);
            let col_j = pattern.get_column(j);

            println!("{}, {}", i, j);

            if col_i != col_j {
                is_symmetric = false;
            }

            j += 1;
        }

        if is_symmetric {
            vertical = x_axis + 1;
            break;
        }
    }

    let mut horizontal = 0;

    for y_axis in get_rows_axis(&pattern).iter() {
        println!("Possible horizontal symmetry axis at: {}", y_axis);

        let mut j = y_axis + 1;
        let mut is_symmetric = true;

        for i in (0 .. y_axis + 1).rev() {
            // it could happen, that we go beyond the bottom border
            if j >= pattern.height() {
                break;
            }

            let row_i = pattern.get_row(i);
            let row_j = pattern.get_row(j);

            println!("{}, {}", i, j);

            if row_i != row_j {
                is_symmetric = false;
            }

            j += 1;
        }

        if is_symmetric {
            horizontal = y_axis + 1;
            break;
        }
    }

    (vertical, horizontal)
}

fn get_cols_axis(pattern: &TextMap) -> Vec<usize> {
    let mut cols_axis = vec![];

    for x in 0 .. pattern.width() - 1 {
        let col_0 = pattern.get_column(x);
        let col_1 = pattern.get_column(x + 1);

        if col_0 == col_1 {
            cols_axis.push(x);
        }
    }

    cols_axis
}

fn get_rows_axis(pattern: &TextMap) -> Vec<usize> {
    let mut rows_axis = vec![];

    for y in 0 .. pattern.height() - 1 {
        let row_0 = pattern.get_row(y);
        let row_1 = pattern.get_row(y + 1);

        if row_0 == row_1 {
            rows_axis.push(y);
        }
    }

    rows_axis
}

