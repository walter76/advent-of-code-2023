// const EXAMPLE_INIT_SEQUENCE: &str = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

fn main() {
    println!("The hash value of 'HASH' is: {}", calculate_hash("HASH"));

    // sum_of_hashes(EXAMPLE_INIT_SEQUENCE);

    let input = std::fs::read_to_string("input.txt").unwrap();
    sum_of_hashes(&input);
}

fn sum_of_hashes(input: &str) {
    let mut sum_of_hashes: i64 = 0;

    for s in input.split(',') {
        let hash = calculate_hash(s);

        println!("- '{}' becomes {}", s, hash);

        sum_of_hashes += hash as i64;
    }

    println!();
    println!("Sum of hashes: {}", sum_of_hashes);
}

fn calculate_hash(s: &str) -> u8 {
    let mut hash_value: u32 = 0;

    for c in s.chars() {
        if c == '\n' {
            continue;
        }

        let ascii_code = char_to_ascii(c);

        hash_value += ascii_code as u32;
        hash_value *= 17;
        hash_value = hash_value % 256;
    }

    hash_value as u8
}

fn char_to_ascii(c: char) -> u8 {
    c as u8
}

#[cfg(test)]
mod tests {
    use crate::{char_to_ascii, calculate_hash};

    #[test]
    fn char_to_ascii_should_return_72_for_uppercase_h() {
        assert_eq!(72, char_to_ascii('H'));
    }

    #[test]
    fn char_to_ascii_should_return_65_for_uppercase_a() {
        assert_eq!(65, char_to_ascii('A'));
    }

    #[test]
    fn char_to_ascii_should_return_61_for_equals() {
        assert_eq!(61, char_to_ascii('='));
    }

    #[test]
    fn char_to_ascii_should_return_45_for_minus() {
        assert_eq!(45, char_to_ascii('-'));
    }

    #[test]
    fn calculate_hash_should_return_52_for_uppercase_hash() {
        assert_eq!(52, calculate_hash("HASH"));
    }
}
