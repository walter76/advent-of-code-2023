use std::fmt::Display;

use crate::pos::Pos;

/// Sorts the vector of bricks by z-order using the start coordinate.
///
/// # Arguments
///
/// - `bricks` - vector with bricks to be sorted
pub fn sort_bricks_by_z_order(mut bricks: Vec<Brick>) -> Vec<Brick> {
    bricks.sort_by(|brick_a, brick_b| {
        brick_a.start_pos().z().partial_cmp(&brick_b.start_pos().z()).unwrap()
    });

    bricks
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Orientation {
    Horizontal,
    Vertical,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Brick {
    start_pos: Pos,
    end_pos: Pos,
}

impl Display for Brick {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}~{}", self.start_pos, self.end_pos)
    }
}

impl Brick {
    pub fn from_str(brick_text: &str) -> Self {
        let chunks: Vec<_> = brick_text.split('~').collect();

        let pos_0 = Pos::from_str(chunks[0]);
        let pos_1 = Pos::from_str(chunks[1]);

        // if the brick is upside-down, we switch start and end pos
        if pos_0.z() > pos_1.z() {
            Self {
                start_pos: pos_1,
                end_pos: pos_0,
            }
        } else {
            Self {
                start_pos: pos_0,
                end_pos: pos_1,
            }
        }

    }

    pub fn start_pos(&self) -> &Pos {
        &self.start_pos
    }

    pub fn end_pos(&self) -> &Pos {
        &self.end_pos
    }

    pub fn orientation(&self) -> Orientation {
        if self.start_pos.z() == self.end_pos.z() {
            Orientation::Horizontal
        } else {
            Orientation::Vertical
        }
    }

    pub fn intersects(&self, other_brick: &Brick) -> bool {
    }
}

#[cfg(test)]
mod tests {
    use crate::{brick::{Brick, Orientation, sort_bricks_by_z_order}, pos::Pos};

    #[test]
    fn sort_bricks_by_z_order_should_sort_bricks() {
        let brick_a = Brick::from_str(r"0,1,6~2,1,6");
        let brick_b = Brick::from_str(r"1,0,1~1,2,1");
        let unsorted_bricks = vec![brick_a.clone(), brick_b.clone()];

        assert_eq!(vec![brick_b, brick_a], sort_bricks_by_z_order(unsorted_bricks));
    }

    const BRICK_1: &str = r"1,0,1~1,2,1";

    #[test]
    fn from_str_should_parse_brick_1() {
        assert_eq!(
            Brick {
                start_pos: Pos::new(1,0,1),
                end_pos: Pos::new(1,2,1),
            },
            Brick::from_str(BRICK_1)
        );
    }

    const BRICK_VERTICAL: &str = r"0,0,1~0,0,10";

    #[test]
    fn orientation_should_be_vertical_for_vertical_brick() {
        assert_eq!(Orientation::Vertical, Brick::from_str(BRICK_VERTICAL).orientation());
    }

    const BRICK_HORIZONTAL: &str = r"0,0,10~1,0,10";

    #[test]
    fn orientation_should_be_horizontal_for_horizontal_brick() {
        assert_eq!(Orientation::Horizontal, Brick::from_str(BRICK_HORIZONTAL).orientation());
    }
}

