use std::fmt::Display;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Pos {
    x: i64,
    y: i64,
    z: i64,
}

impl Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(x={}, y={}, z={})", self.x, self.y, self.z)
    }
}

impl Pos {
    pub fn new(x: i64, y: i64, z: i64) -> Self {
        Self { x, y, z, }
    }

    pub fn from_str(pos_text: &str) -> Self {
        let chunks: Vec<_> = pos_text.split(',').collect();

        let x = chunks[0].parse::<i64>().unwrap();
        let y = chunks[1].parse::<i64>().unwrap();
        let z = chunks[2].parse::<i64>().unwrap();

        Self { x, y, z, }
    }

    pub fn x(&self) -> i64 {
        self.x
    }

    pub fn y(&self) -> i64 {
        self.y
    }

    pub fn z(&self) -> i64 {
        self.z
    }
}

#[cfg(test)]
mod tests {
    use crate::pos::Pos;

    #[test]
    fn from_str_should_parse_0_0_0() {
        assert_eq!(Pos { x: 0, y: 0, z: 0}, Pos::from_str("0,0,0"));
    }

    #[test]
    fn from_str_should_parse_1_1_8() {
        assert_eq!(Pos { x: 1, y: 1, z: 8}, Pos::from_str("1,1,8"));
    }
}

