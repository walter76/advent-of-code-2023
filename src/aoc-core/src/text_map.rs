use std::{slice::Iter, fmt::Display};

use crate::tile::Tile;

/// Accessor for the neighbor in a certain direction
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Direction {
    NorthWest,
    North,
    NorthEast,
    West,
    Center,
    East,
    SouthWest,
    South,
    SouthEast,
}

/// Neighbors of a point in the TextMap.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Neighbors {
    data: Vec<Option<char>>,
    pos_x: usize,
    pos_y: usize,
}

impl Display for Neighbors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}, {:?}, {:?}\n", self.data[0], self.data[1], self.data[2])?;
        write!(f, "{:?}, {:?}, {:?}\n", self.data[3], self.data[4], self.data[5])?;
        write!(f, "{:?}, {:?}, {:?}", self.data[6], self.data[7], self.data[8])
    }
}

impl Neighbors {
    /// Create a new set of neighbors from the `text_map` at the position `pos_x, pos_y`.
    ///
    /// # Arguments
    ///
    /// `pos_x` - x-coordinate of the center
    /// `pos_y` - y-coordinate of the center
    /// `text_map` - the text map to get the neighbors from
    pub fn new(pos_x: usize, pos_y: usize, text_map: &TextMap) -> Self {
        let mut data: Vec<Option<char>> = vec![None; 9];

        // TODO: optimize this by doing the edge cases only if required and in all other cases just
        // copy the chars directly

        // the upper row
        if pos_y > 0 {
            if pos_x > 0 {
                data[0] = Some(text_map.char_at(pos_x - 1, pos_y - 1));
            }

            data[1] = Some(text_map.char_at(pos_x, pos_y - 1));

            if pos_x < text_map.width() - 1 {
                data[2] = Some(text_map.char_at(pos_x + 1, pos_y - 1));
            }
        }

        // the center
        if pos_x > 0 {
            data[3] = Some(text_map.char_at(pos_x - 1, pos_y));
        }

        data[4] = Some(text_map.char_at(pos_x, pos_y));

        if pos_x < text_map.width() - 1 {
            data[5] = Some(text_map.char_at(pos_x + 1, pos_y));
        }

        // the lower row
        if pos_y < text_map.height() - 1 {
            if pos_x > 0 {
                data[6] = Some(text_map.char_at(pos_x - 1, pos_y + 1));
            }

            data[7] = Some(text_map.char_at(pos_x, pos_y + 1));

            if pos_x < text_map.width() - 1 {
                data[8] = Some(text_map.char_at(pos_x + 1, pos_y + 1));
            }
        }

        Self {
            data,
            pos_x,
            pos_y,
        }
    }

    /// Returns the neighbor at a specific direction.
    ///
    /// # Arguments
    ///
    /// `direction` - the direction the neighbor can be found
    pub fn get(&self, direction: Direction) -> Option<char> {
        match direction {
            Direction::NorthWest => self.data[0],
            Direction::North => self.data[1],
            Direction::NorthEast => self.data[2],
            Direction::West => self.data[3],
            Direction::Center => self.data[4],
            Direction::East => self.data[5],
            Direction::SouthWest => self.data[6],
            Direction::South => self.data[7],
            Direction::SouthEast => self.data[8],
        }
    }

    /// Returns an iterator through all the neighbors, starting from the upper left.
    pub fn iter(&self) -> Iter<Option<char>> {
        self.data.iter()
    }

    /// Returns the center coordinates of the neighbors field.
    pub fn center(&self) -> (usize, usize) {
        (self.pos_x, self.pos_y)
    }

    /// Calculates the position of the neighbor from the center.
    ///
    /// # Arguments
    ///
    /// `direction` - the direction of the neighbor
    pub fn pos_of(&self, direction: Direction) -> Option<(usize, usize)> {
        match direction {
            Direction::NorthWest => if self.data[0].is_some() {
                Some((self.pos_x - 1, self.pos_y - 1))
            } else {
                None
            }

            Direction::North => if self.data[1].is_some() {
                Some((self.pos_x, self.pos_y - 1))
            } else {
                None
            }

            Direction::NorthEast => if self.data[2].is_some() {
                Some((self.pos_x + 1, self.pos_y - 1))
            } else {
                None
            }

            Direction::West => if self.data[3].is_some() {
                Some((self.pos_x - 1, self.pos_y))
            } else {
                None
            }

            Direction::Center => Some((self.pos_x, self.pos_y)),

            Direction::East => if self.data[5].is_some() {
                Some((self.pos_x + 1, self.pos_y))
            } else {
                None
            }

            Direction::SouthWest => if self.data[6].is_some() {
                Some((self.pos_x - 1, self.pos_y + 1))
            } else {
                None
            }

            Direction::South => if self.data[7].is_some() {
                Some((self.pos_x, self.pos_y + 1))
            } else {
                None
            }

            Direction::SouthEast => if self.data[8].is_some() {
                Some((self.pos_x + 1, self.pos_y + 1))
            } else {
                None
            }
        }
    }
}

/// Represents a map of characters with dimensions `width` by `height`.
///
/// Each point in the map is represented by a character. It is possible to define a vector of tiles
/// which can be used to do further processing of the map points.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TextMap {
    data: Vec<char>,
    width: usize,
    height: usize,
    tiles: Vec<Tile>,
}

impl Display for TextMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output: Vec<String> = vec![];

        let mut x_axis = "  ".to_string();

        for x in 0 .. self.width {
            x_axis.push_str(&format!("{}", x % 10));
        }

        output.push(x_axis);
        output.push(String::new());

        let mut y = 0;

        for row in self.data.chunks(self.width) {
            let s: String = row.iter().collect();
            output.push(format!("{} {}", y % 10, s));

            y += 1;
        }

        write!(f, "{}", output.join("\n"))
    }
}

impl TextMap {
    /// Create an empty map.
    ///
    /// Each position is initialized with ` ` (space) as a character.
    ///
    /// # Arguments
    ///
    /// - `width` - width of the map
    /// - `height` - height of the map
    /// - `tiles` - set of tiles (can be an empty vector)
    pub fn new(width: usize, height: usize, tiles: Vec<Tile>) -> Self {
        let data = vec![ ' '; width * height ];

        Self {
            width,
            height,
            tiles,
            data,
        }
    }

    /// Create a `TextMap` from a string where each line represents a row on the map. The lines are
    /// separated with end-of-line character. The width is determined by the length of a line,
    /// while the height is determined by counting the numer of lines.
    ///
    /// # Arguments
    ///
    /// - `text` - text as input for creating the `TextMap`
    pub fn from_str(text: &str) -> Self {
        let mut data = vec![];

        for line in text.lines() {
            for c in line.chars() {
                data.push(c);
            }
        }

        let width = text.lines().next().unwrap().len();
        let height = text.lines().count();

        Self {
            data,
            width,
            height,
            tiles: vec![],
        }
    }

    /// Returns the widht of the map.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Returns the height of the map.
    pub fn height(&self) -> usize {
        self.height
    }

    /// Calculates the index from the coordinates to be used to access the character at the
    /// position.
    ///
    /// # Arguments
    ///
    /// - `x` - x-coordinate of the character
    /// - `y` - y-coordinate of the character
    pub fn index_of(&self, x: usize, y: usize) -> usize {
        y * self.width + x
    }

    /// Returns the character at the index.
    ///
    /// # Arguments
    ///
    /// - `index` - index of the character
    pub fn char_by_index(&self, index: usize) -> char {
        self.data[index]
    }

    /// Returns the character at the position.
    ///
    /// # Arguments
    ///
    /// - `x` - x-coordinate of the character
    /// - `y` - y-coordinate of the character
    pub fn char_at(&self, x: usize, y: usize) -> char {
        self.data[self.index_of(x, y)]
    }

    /// Set the character at the position.
    ///
    /// # Arguments
    ///
    /// - `x` - x-coordinate of the character
    /// - `y` - y-coordinate of the character
    /// - `c` - the character to set at the position
    pub fn set_char_at(&mut self, x: usize, y: usize, c: char) {
        let index = self.index_of(x, y);

        self.data[index] = c;
    }

    /// Returns an iterator for the map.
    pub fn iter(&self) -> Iter<char> {
        self.data.iter()
    }

    /// Extract a string by index coordinates. The range end is exclusive, i. e. the character at
    /// `end_index` is not extracted.
    ///
    /// # Arguments
    ///
    /// - `start_index` - the start index
    /// - `end_index` - the end index
    pub fn sub_string_by_index(&self, start_index: usize, end_index: usize) -> String {
        self.data[start_index .. end_index].iter().collect()
    }

    /// Find the xy-coordinates of a character in the map. This function returns a tuple `(x,y)`.
    ///
    /// # Arguments
    ///
    /// - `c` - the character to look for
    pub fn find_pos(&self, c: char) -> (usize, usize) {
        let index = self.data.iter().position(|map_c| *map_c == c).unwrap();

        let y = index / self.width;
        let x = index % self.width;

        (x, y)
    }

    /// Add the definition of a tile to the map. This can be later on used for further processing,
    /// e.g. path-finding with Dijkstra maps.
    ///
    /// # Arguments
    ///
    /// - `tile` - the tile to be stored alongside the map
    pub fn push_tile(&mut self, tile: Tile) {
        self.tiles.push(tile);
    }

    /// Provide a iterator through the set of tiles.
    pub fn iter_tiles(&self) -> Iter<Tile> {
        self.tiles.iter()
    }

    /// Return the row at the y-coordinate.
    ///
    /// # Arguments
    ///
    /// - `y` - the y-coordinate of the row
    pub fn get_row(&self, y: usize) -> Vec<char> {
        let start_index = self.index_of(0, y);

        self.data[ start_index .. start_index + self.width ].iter().map(|c| *c).collect()
    }

    /// Return the column at the x-coordinate.
    ///
    /// # Arguments
    ///
    /// - `x` - the x-coordinate of the column
    pub fn get_column(&self, x: usize) -> Vec<char> {
        let mut column: Vec<char> = vec![];

        for y in 0 .. self.height {
            column.push(self.data[ x + y * self.width ]);
        }

        column
    }

    /// Replace all occurences of `search_char` with `replacement`.
    ///
    /// # Arguments
    ///
    /// - `search_char` - the character to search for replacement
    /// - `replacement` - the character to replace it with
    pub fn replace_all(&mut self, search_char: char, replacement: char) {
        for c in self.data.iter_mut().filter(|c| **c == search_char) {
            *c = replacement;
        }
    }

    /// Fills the complete map with `c`.
    ///
    /// # Arguments
    ///
    /// - `c` - the character to fill the map with
    pub fn fill(&mut self, c: char) {
        self.data.fill(c);
    }

    /// Gets the 3 x 3 neighbors of a position in the text map.
    ///
    /// # Arguments
    ///
    /// - `x` - x-coordinate of the center
    /// - `y` - y-coordinate of the center
    pub fn neighbors(&self, x: usize, y: usize) -> Neighbors {
        Neighbors::new(x, y, self)
    }

    /// Determines x- and y-coordinates from a index.
    ///
    /// # Arguments
    ///
    /// - `index` - the index into the data array
    pub fn pos_of(&self, index: usize) -> (usize, usize) {
        let x = index % self.width;
        let y = index / self.width;

        (x, y)
    }
}

#[cfg(test)]
mod tests {
    use crate::text_map::{TextMap, Neighbors, Direction};

    const EXAMPLE_MAP: &str = r"467..114..
...*......
..35..633x
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    #[test]
    fn text_map_from_str_should_calculate_10_as_width() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(10, text_map.width());
    }

    #[test]
    fn text_map_from_str_should_calculate_10_as_height() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(10, text_map.height());
    }

    #[test]
    fn text_map_index_of_should_return_0() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(0, text_map.index_of(0, 0));
    }

    #[test]
    fn text_map_index_of_should_return_7() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(7, text_map.index_of(7, 0));
    }

    #[test]
    fn text_map_index_of_should_return_10() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(10, text_map.index_of(0, 1));
    }

    #[test]
    fn text_map_index_of_should_return_15() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(15, text_map.index_of(5, 1));
    }

    #[test]
    fn text_map_index_of_should_return_99() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!(99, text_map.index_of(9, 9));
    }

    #[test]
    fn find_pos_of_dollar_should_return_3_8() {
        let text_map = TextMap::from_str(EXAMPLE_MAP);

        assert_eq!((3, 8), text_map.find_pos('$'));
    }

    const EXAMPLE_PATTERN_1: &str = r"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.";

    #[test]
    fn get_row_should_return_row() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);

        assert_eq!(vec![ '#', '.', '#', '#', '.', '.', '#', '#', '.', ], text_map.get_row(0));
    }

    #[test]
    fn get_column_should_return_column() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);

        assert_eq!(vec![ '.', '#', '.', '.', '#', '.', '#', ], text_map.get_column(4));
    }

    const EXAMPLE_PATTERN_1_OUTPUT: &str = r"  012345678

0 #.##..##.
1 ..#.##.#.
2 ##......#
3 ##......#
4 ..#.##.#.
5 ..##..##.
6 #.#.##.#.";

    #[test]
    fn display_should_format_output() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);

        assert_eq!(EXAMPLE_PATTERN_1_OUTPUT, format!("{}", text_map));
    }

    #[test]
    fn neighbors_0_0() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    None, None, None,
                    None, Some('#'), Some('.'),
                    None, Some('.'), Some('.'),
                ],
                pos_x: 0,
                pos_y: 0,
            },
            text_map.neighbors(0, 0)
        );
    }

    #[test]
    fn neighbors_4_0() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    None, None, None,
                    Some('#'), Some('.'), Some('.'),
                    Some('.'), Some('#'), Some('#'),
                ],
                pos_x: 4,
                pos_y: 0,
            },
            text_map.neighbors(4, 0)
        );
    }

    #[test]
    fn neighbors_8_0() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    None, None, None,
                    Some('#'), Some('.'), None,
                    Some('#'), Some('.'), None,
                ],
                pos_x: 8,
                pos_y: 0,
            },
            text_map.neighbors(8, 0)
        );
    }

    #[test]
    fn neighbors_0_3() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    None, Some('#'), Some('#'),
                    None, Some('#'), Some('#'),
                    None, Some('.'), Some('.'),
                ],
                pos_x: 0,
                pos_y: 3,
            },
            text_map.neighbors(0, 3)
        );
    }

    #[test]
    fn neighbors_4_3() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    Some('.'), Some('.'), Some('.'),
                    Some('.'), Some('.'), Some('.'),
                    Some('.'), Some('#'), Some('#'),
                ],
                pos_x: 4,
                pos_y: 3,
            },
            text_map.neighbors(4, 3)
        );
    }

    #[test]
    fn neighbors_8_3() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    Some('.'), Some('#'), None,
                    Some('.'), Some('#'), None,
                    Some('#'), Some('.'), None,
                ],
                pos_x: 8,
                pos_y: 3,
            },
            text_map.neighbors(8, 3)
        );
    }

    #[test]
    fn neighbors_0_6() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    None, Some('.'), Some('.'),
                    None, Some('#'), Some('.'),
                    None, None, None,
                ],
                pos_x: 0,
                pos_y: 6,
            },
            text_map.neighbors(0, 6)
        );
    }

    #[test]
    fn neighbors_4_6() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    Some('#'), Some('.'), Some('.'),
                    Some('.'), Some('#'), Some('#'),
                    None, None, None,
                ],
                pos_x: 4,
                pos_y: 6,
            },
            text_map.neighbors(4, 6)
        );
    }

    #[test]
    fn neighbors_8_6() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        
        assert_eq!(
            Neighbors {
                data: vec![
                    Some('#'), Some('.'), None,
                    Some('#'), Some('.'), None,
                    None, None, None,
                ],
                pos_x: 8,
                pos_y: 6,
            },
            text_map.neighbors(8, 6)
        );
    }

    #[test]
    fn neighbors_get_north_west() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('#'), neighbors.get(Direction::NorthWest));
    }

    #[test]
    fn neighbors_get_north() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('#'), neighbors.get(Direction::North));
    }

    #[test]
    fn neighbors_get_north_east() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('.'), neighbors.get(Direction::NorthEast));
    }

    #[test]
    fn neighbors_get_west() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('#'), neighbors.get(Direction::North));
    }

    #[test]
    fn neighbors_get_center() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('.'), neighbors.get(Direction::Center));
    }

    #[test]
    fn neighbors_get_east() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('#'), neighbors.get(Direction::East));
    }

    #[test]
    fn neighbors_get_south_west() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('.'), neighbors.get(Direction::SouthWest));
    }

    #[test]
    fn neighbors_get_south() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('.'), neighbors.get(Direction::South));
    }

    #[test]
    fn neighbors_get_south_east() {
        let text_map = TextMap::from_str(EXAMPLE_PATTERN_1);
        let neighbors = text_map.neighbors(1, 4);

        assert_eq!(Some('#'), neighbors.get(Direction::SouthEast));
    }
}

