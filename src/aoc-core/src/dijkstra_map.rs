use std::fmt::Display;

use crate::text_map::TextMap;

const UNREACHABLE_TILE: i32 = -8000;

/// Dijkstra map implementation to be used with `TextMap`.
///
/// The implementation is only able to walk from one starting position. Usually a Dijkstra map
/// would be created by walking from multiple starting positions. As of today, this was not
/// required.
///
/// https://www.gamedev.net/tutorials/programming/artificial-intelligence/dijkstras-algorithm-shortest-path-r3872/
#[derive(Default, Debug, Clone, Eq, PartialEq)]
pub struct DijkstraMap {
    data: Vec<i32>,
    width: usize,
    height: usize,
    max_distance: i32,
}

impl From<&TextMap> for DijkstraMap {
    fn from(text_map: &TextMap) -> Self {
        let mut dijkstra_map = Self::default();

        // TODO: need to detect starting position or pass it in
        dijkstra_map.create(text_map, 1, 1);

        dijkstra_map
    }
}

impl Display for DijkstraMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for row in self.data.chunks(self.width) {
            let s: Vec<String> = row.iter().map(|v| format!("{}", v)).collect();
            write!(f, "{}\n", s.join(" "))?;
        }

        Ok(())
    }
}

impl DijkstraMap {
    /// Creates a Dijkstra map by walking the possible paths from a starting position. This method
    /// will delete any previously created Dijkstra map.
    ///
    /// It will only walk paths horizontally and vertically; walking diagonally is not supported.
    ///
    /// # Arguments
    ///
    /// - `text_map` - the `TextMap` to be used for creating the Dijkstra map
    /// - `start_x` - the x starting position
    /// - `start_y` - the y starting position
    pub fn create(&mut self, text_map: &TextMap, start_x: usize, start_y: usize) {
        self.data = vec![UNREACHABLE_TILE; text_map.width() * text_map.height()];
        self.width = text_map.width();
        self.height = text_map.height();
        self.max_distance = 0;

        self.init_starting_position(start_x, start_y);

        print!("{}", self);

        let mut distance: i32 = 0;

        while self.walk_path(distance, &text_map) {
            println!("{}", self);

            distance += 1;
        }

        self.max_distance = distance;
    }

    /// Returns the maximum distance for the path that has been walked recently.
    pub fn max_distance(&self) -> i32 {
        self.max_distance
    }

    /// Returns `true`, if this instance of the Dijkstra map is having a loop from the latest
    /// starting position.
    pub fn is_loop(&self) -> bool {
        // find the starting position
        let mut index = self.data.iter().position(|v| *v == 0).unwrap();

        // we check clock-wise, starting from north
        // we can either be on the way back (distance - 1) or on the way toward the farthest point
        // (distance + 1)

        let mut nodes_visited = vec![false; self.width * self.height];
        let mut max_iterations = 0;
        let mut walking_direction = 1;

        loop {
            if max_iterations == 10 {
                return false;
            }

            nodes_visited[index] = true;

            let distance = self.data[index];
            if distance == self.max_distance {
                walking_direction = -1;
            }

            println!("{}: {}; {}", index, distance, walking_direction);

            max_iterations += 1;

            if !nodes_visited[index - self.width]
                && self.data[index - self.width] == distance + walking_direction
            {
                // check the north and walk there if possible
                index -= self.width;

                continue;
            } else if !nodes_visited[index + 1]
                && self.data[index + 1] == distance + walking_direction
            {
                // check the east and walk there if possible
                index += 1;

                continue;
            } else if !nodes_visited[index + self.width]
                && self.data[index + self.width] == distance + walking_direction
            {
                // check the south and walk there if possible
                index += self.width;
                
                continue;
            } else if !nodes_visited[index - 1]
                && self.data[index - 1] == distance + walking_direction
            {
                // check the west and walk there if possible
                index -= 1;
                
                continue;
            } else {
                // there is no further way to walk to we do not have a loop
                return self.data[index - self.width] == 0
                    || self.data[index + 1] == 0
                    || self.data[index + self.width] == 0
                    || self.data[index - 1] == 0;
            }
        }
    }

    fn init_starting_position(&mut self, x: usize, y: usize) {
        let index = self.index_of(x, y); 
        self.data[index] = 0;
    }

    fn index_of(&self, x: usize, y: usize) -> usize {
        y * self.width + x
    }

    fn pos_of(&self, index: usize) -> (usize, usize) {
        let y = index / self.width;
        let x = index % self.width;

        (x, y)
    }

    fn walk_path(&mut self, distance: i32, text_map: &TextMap) -> bool {
        let mut next_iteration = false;

        let positions_to_check: Vec<_> = self.data.iter()
            .enumerate()
            .filter(|(_, value)| ** value == distance)
            .map(|(index, _)| self.pos_of(index))
            .collect();

        for (x, y) in positions_to_check.iter() {
            let index = self.index_of(*x, *y);

            let c = text_map.char_at(*x, *y);
            let tile = text_map.iter_tiles().find(|tile| tile.glyph() == c).unwrap();

            // FIXME: We also need to check if the tile at the possible exit can connect back, e.g.
            // if this tile connects north, the tile above, needs to have a connection south. Only
            // then we set the new value.

            // check north
            if tile.possible_exits[1] > 0 && *y > 0 {
                if self.data[index - self.width] < 0 {
                    self.data[index - self.width] = distance + 1;

                    next_iteration = true;
                }
            }

            // check east
            if tile.possible_exits[5] > 0 && *x < (self.width - 1) {
                if self.data[index + 1] < 0 {
                    self.data[index + 1] = distance + 1;

                    next_iteration = true;
                }
            }

            // check south
            if tile.possible_exits[7] > 0 && *y < (self.height - 1) {
                if self.data[index + self.width] < 0 {
                    self.data[index + self.width] = distance + 1;

                    next_iteration = true;
                }
            }
 
            // check west
            if tile.possible_exits[3] > 0 && *x > 0 {
                if self.data[index - 1] < 0 {
                    self.data[index - 1] = distance + 1;

                    next_iteration = true;
                }
            }
       }

        next_iteration
    }
}

#[cfg(test)]
mod tests {
    use crate::{text_map::TextMap, tile::Tile};

    use super::DijkstraMap;

    const EXAMPLE_MAP: &str = r".....
.F-7.
.|.|.
.L-J.
.....";

    fn create_text_map(text: &str) -> TextMap {
        let mut text_map = TextMap::from_str(text);

        // TODO: This needs to be carved out to day_10

        // `|` is a _vertical pipe_ connecting north and south.
        text_map.push_tile(
            Tile::new(
                '|',
                [
                    0, 1, 0,
                    0, 0, 0,
                    0, 1, 0,
                ]
            )
        );

        // `-` is a _horizontal pipe_ connecting east and west.
        text_map.push_tile(
            Tile::new(
                '-',
                [
                    0, 0, 0,
                    1, 0, 1,
                    0, 0, 0,
                ]
            )
        );

        // `L` is a _90-degree bend pipe_ connecting north and east.
        text_map.push_tile(
            Tile::new(
                'L',
                [
                    0, 1, 0,
                    0, 0, 1,
                    0, 0, 0,
                ]
            )
        );

        // `J` is a _90-degree bend pipe_ connecting north and west.
        text_map.push_tile(
            Tile::new(
                'J',
                [
                    0, 1, 0,
                    1, 0, 0,
                    0, 0, 0,
                ]
            )
        );

        // `7` is a _90-degree bend pipe_ connecting south and west.
        text_map.push_tile(
            Tile::new(
                '7',
                [
                    0, 0, 0,
                    1, 0, 0,
                    0, 1, 0,
                ]
            )
        );

        // `F` is a _90-degree bend pipe_ connecting south and east.
        text_map.push_tile(
            Tile::new(
                'F',
                [
                    0, 0, 0,
                    0, 0, 1,
                    0, 1, 0,
                ]
            )
        );

        // `.` is _ground_; there is no pipe in this tile.
        text_map.push_tile(
            Tile::new(
                '.',
                [
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0,
                ]
            )
        );

        // `S` is the _starting position_ of the animal; there is a pipe on this tile, but your
        // sketch doesn't show what shape the pipe has.
        text_map.push_tile(
            Tile::new(
                'S',
                [
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0,
                ]
            )
        );

        text_map
    }

    #[test]
    fn from_text_map_should_set_starting_position_to_0() {
        let example_map = create_text_map(EXAMPLE_MAP);
        let dijkstra_map = DijkstraMap::from(&example_map);

        assert_eq!(0, dijkstra_map.data[dijkstra_map.index_of(1, 1)]);
    }

    #[test]
    fn simple_map_should_create_one_simple_distance_loop() {
        let example_map = create_text_map(EXAMPLE_MAP);
        let dijkstra_map = DijkstraMap::from(&example_map);

        assert_eq!(
            vec![
                -8000, -8000, -8000, -8000, -8000,
                -8000,     0,     1,     2, -8000,
                -8000,     1, -8000,     3, -8000,
                -8000,     2,     3,     4, -8000,
                -8000, -8000, -8000, -8000, -8000,
            ],
            dijkstra_map.data
        );
    }

    #[test]
    fn simple_map_with_f_should_have_loop() {
        let example_map = create_text_map(EXAMPLE_MAP);
        let dijkstra_map = DijkstraMap::from(&example_map);

        assert!(dijkstra_map.is_loop());
    }

    #[test]
    fn simple_map_with_bar_should_not_have_loop() {
        let mut example_map = create_text_map(EXAMPLE_MAP);
        example_map.set_char_at(1, 1, '|');

        let dijkstra_map = DijkstraMap::from(&example_map);

        assert!(!dijkstra_map.is_loop());
    }

    const SIMPLE_MAP_WITH_NOISE: &str = r"-L|F7
7F-7|
L|7||
-L-J|
L|-JF";

   #[test]
    fn simple_map_with_noise_should_create_one_simple_distance_loop() {
        let example_map = create_text_map(SIMPLE_MAP_WITH_NOISE);
        let dijkstra_map = DijkstraMap::from(&example_map);

        assert_eq!(
            vec![
                -8000, -8000, -8000, -8000, -8000,
                -8000,     0,     1,     2, -8000,
                -8000,     1, -8000,     3, -8000,
                -8000,     2,     3,     4, -8000,
                -8000, -8000, -8000, -8000, -8000,
            ],
            dijkstra_map.data
        );
    }
}

