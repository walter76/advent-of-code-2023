#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Tile {
    glyph: char,
    pub possible_exits: [i8; 9],
}

impl Tile {
    pub fn new(glyph: char, possible_exits: [i8; 9]) -> Self {
        Self {
            glyph,
            possible_exits,
        }
    }

    pub fn glyph(&self) -> char {
        self.glyph
    }
}

