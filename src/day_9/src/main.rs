const EXAMPLE_SENSOR_READING_1: &str = "0 3 6 9 12 15";

const EXAMPLE: &str = r"0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

fn main() {
    println!("{}", calc_extrapolated_value(EXAMPLE_SENSOR_READING_1, false));

    println!();

    calc_sum_of(EXAMPLE, false);

    println!();

    let input = std::fs::read_to_string("input.txt").unwrap();
    calc_sum_of(&input, false);

    println!();

    calc_sum_of(EXAMPLE, true);

    println!();
    calc_sum_of(&input, true);
}

fn calc_sum_of(text: &str, backwards: bool) {
    let mut sum_of: i64 = 0;
    for line in text.lines() {
        let extrapolated_value = calc_extrapolated_value(line, backwards);

        sum_of += extrapolated_value;

        println!("{} ({})", extrapolated_value, sum_of);
    }
}

fn calc_extrapolated_value(sensor_reading_text: &str, backwards: bool) -> i64 {
    let sensor_reading = SensorReading::from_str(sensor_reading_text);
    let history = if backwards {
        create_history_backwards(sensor_reading)
    } else {
        create_history(sensor_reading)
    };

    get_extrapolated_value(&history, backwards)
}

fn create_history(sensor_reading: SensorReading) -> Vec<SensorReading> {
    let mut history: Vec<SensorReading> = vec![];

    // create the history

    history.push(sensor_reading);

    let mut previous_sensor_reading = history.last().unwrap();
    while !previous_sensor_reading.is_zero_readings() {
        history.push(previous_sensor_reading.differences());

        previous_sensor_reading = history.last().unwrap(); 
        println!("{:?}", previous_sensor_reading);
    }

    println!("{:?}", history);

    // extrapolate the missing values in the history 

    let mut previously_extrapolated_value = 0;
    for sensor_reading in history.iter_mut().rev() {
        let last_value_in_sensor_reading = sensor_reading.last();
        let extrapolated_value = previously_extrapolated_value + last_value_in_sensor_reading;

        sensor_reading.push(extrapolated_value);

        previously_extrapolated_value = extrapolated_value;
    }

    println!("{:?}", history);

    history
}

fn create_history_backwards(sensor_reading: SensorReading) -> Vec<SensorReading> {
    let mut history: Vec<SensorReading> = vec![];

    // create the history

    history.push(sensor_reading);

    let mut previous_sensor_reading = history.last().unwrap();
    while !previous_sensor_reading.is_zero_readings() {
        history.push(previous_sensor_reading.differences());

        previous_sensor_reading = history.last().unwrap(); 
        println!("{:?}", previous_sensor_reading);
    }

    println!("{:?}", history);

    // extrapolate the missing values in the history BACKWARDS 

    let mut previously_extrapolated_value = 0;
    for sensor_reading in history.iter_mut().rev() {
        let first_value_in_sensor_reading = sensor_reading.first();
        let extrapolated_value = first_value_in_sensor_reading - previously_extrapolated_value;

        sensor_reading.push_front(extrapolated_value);

        previously_extrapolated_value = extrapolated_value;
    }

    println!("{:?}", history);

    history
}

fn get_extrapolated_value(history: &[SensorReading], backwards: bool) -> i64 {
    if backwards {
        history.first().unwrap().first()
    } else {
        history.first().unwrap().last()
    }
}

#[derive(Debug, Default)]
struct SensorReading {
    readings: Vec<i64>,
}

impl SensorReading {
    pub fn from_str(sensor_reading: &str) -> Self {
        let readings: Vec<_> = sensor_reading.split_whitespace()
            .map(|r| r.parse::<i64>().unwrap()).collect();

        Self {
            readings,
        }
    }

    pub fn differences(&self) -> Self {
        let mut differences_of_readings: Vec<i64> = vec![]; 

        let mut index: usize = 0;

        while index < self.readings.len() - 1 {
            differences_of_readings.push(self.readings[index + 1] - self.readings[index]);

            index += 1;
        }

        Self {
            readings: differences_of_readings,
        }
    }

    pub fn is_zero_readings(&self) -> bool {
        self.readings.iter().all(|r| *r == 0)
    }

    pub fn push(&mut self, reading: i64) {
        self.readings.push(reading);
    }

    pub fn last(&self) -> i64 {
        *self.readings.last().unwrap()
    }

    pub fn first(&self) -> i64 {
        *self.readings.first().unwrap()
    }
    
    pub fn push_front(&mut self, reading: i64) {
        self.readings.insert(0, reading);
    }
}

#[cfg(test)]
mod tests {
}
