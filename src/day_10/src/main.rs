const SIMPLE_EXAMPLE_MAP: &str = r".....
.F-7.
.|.|.
.L-J.
.....";

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use aoc_core::text_map::TextMap;

    use crate::SIMPLE_EXAMPLE_MAP;

    #[test]
    fn simple_example_map_can_be_parsed_by_text_map() {
        let simple_map = TextMap::from_str(SIMPLE_EXAMPLE_MAP);

        assert_eq!(5, simple_map.width());
        assert_eq!(5, simple_map.height());
    }
}

