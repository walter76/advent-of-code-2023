use std::{collections::HashMap, cmp::Ordering};

use once_cell::sync::Lazy;

    const EXAMPLE: &str = r"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

fn main() {
    // let input = EXAMPLE;
    let content = std::fs::read_to_string("input.txt").unwrap();
    let input = content.as_str();

    let mut hands: Vec<HandWithBid> =
        input.lines().map(|l| HandWithBid::from_str(l)).collect();

    hands.sort_by(|hand_a, hand_b| {
        let hand_a_strength = hand_a.hand_type().strength();
        let hand_b_strength = hand_b.hand_type().strength();

        if hand_a_strength < hand_b_strength {
            Ordering::Less
        } else if hand_a_strength == hand_b_strength {
            for (index, card_hand_a) in hand_a.hand.chars().enumerate() {
                let card_hand_a_strength = STRENGTH_OF_CARDS.iter().position(|c| *c == card_hand_a).unwrap();

                let card_hand_b = hand_b.hand.chars().nth(index).unwrap();
                let card_hand_b_strength = STRENGTH_OF_CARDS.iter().position(|c| *c == card_hand_b).unwrap();

                if card_hand_a_strength < card_hand_b_strength {
                    return Ordering::Less
                }
                if card_hand_a_strength > card_hand_b_strength {
                    return Ordering::Greater
                }
            }

            Ordering::Equal
        } else {
            Ordering::Greater
        }
    });

    let mut total_winnings: i64 = 0;
    let mut rank: i64 = 1;

    for hand in hands.iter() {
        total_winnings += rank * hand.bid;
        rank += 1;
    }

    println!("{}", total_winnings);
}

static STRENGTH_OF_CARDS: Lazy<Vec<char>> = Lazy::new(|| {
    vec![
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        'T',
        'J',
        'Q',
        'K',
        'A',
    ]
});

#[derive(Debug, Eq, PartialEq, Clone)]
enum HandType {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPair,
    OnePair,
    HighCard,
}

impl HandType {
    pub fn strength(&self) -> u8 {
        match self {
            HandType::FiveOfAKind => 6,
            HandType::FourOfAKind => 5,
            HandType::FullHouse => 4,
            HandType::ThreeOfAKind => 3,
            HandType::TwoPair => 2,
            HandType::OnePair => 1,
            HandType::HighCard => 0,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct HandWithBid {
    hand: String,
    bid: i64,
}

impl HandWithBid {
    pub fn from_str(hand_with_bid_text: &str) -> Self {
        let hand = hand_with_bid_text[ .. 5 ].to_string();
        let bid = hand_with_bid_text[ 6 .. ].parse::<i64>().unwrap();

        Self {
            hand,
            bid,
        }
    }

    fn hand_type(&self) -> HandType {
        let cards_counted = self.count_cards();
        let number_of_different_cards = cards_counted.keys().len();

        match number_of_different_cards {
            1 => HandType::FiveOfAKind,
            2 => {
                let values: Vec<_> = cards_counted.iter().map(|(_, n)| *n).collect();

                if (values[0] == 1 && values[1] == 4)
                    || (values[0] == 4 && values[1] == 1) {
                    HandType::FourOfAKind
                } else {
                    HandType::FullHouse
                }
            }
            3 => {
                let values: Vec<_> = cards_counted.iter().map(|(_, n)| *n).collect();

                if values[0] == 3 || values[1] == 3 || values[2] == 3 {
                    HandType::ThreeOfAKind
                } else {
                    HandType::TwoPair
                }
            }
            4 => HandType::OnePair,
            _ => HandType::HighCard,
        }
    }

    fn count_cards(&self) -> HashMap<char, usize> {
        let mut cards_counted: HashMap<char, usize> = HashMap::new();

        for c in self.hand.chars() {
            if !cards_counted.contains_key(&c) {
                cards_counted.insert(c, 1);
            } else {
                *cards_counted.get_mut(&c).unwrap() += 1;
            }
        }

        cards_counted
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::Ordering;

    use crate::{HandWithBid, HandType, STRENGTH_OF_CARDS};

    const EXAMPLE_HAND_WITH_BID: &str = "32T3K 765";

    #[test]
    fn hand_with_bid_should_parse() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_WITH_BID);

        assert_eq!("32T3K", hand_with_bid.hand);
        assert_eq!(765, hand_with_bid.bid);
    }

    #[test]
    fn hand_with_bid_count_cards_for_example_hand() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_WITH_BID);
        let cards_counted = hand_with_bid.count_cards();

        assert_eq!(2, *cards_counted.get(&'3').unwrap());
        assert_eq!(1, *cards_counted.get(&'2').unwrap());
        assert_eq!(1, *cards_counted.get(&'T').unwrap());
        assert_eq!(1, *cards_counted.get(&'K').unwrap());
        assert_eq!(4, cards_counted.keys().len());
    }

    const EXAMPLE_HAND_FIVE_OF_A_KIND: &str = "AAAAA 1";

    #[test]
    fn hand_with_bid_hand_type_should_return_five_of_a_kind() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_FIVE_OF_A_KIND);

        assert_eq!(HandType::FiveOfAKind, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_FOUR_OF_A_KIND: &str = "AA8AA 2";

    #[test]
    fn hand_with_bid_hand_type_should_return_four_of_a_kind() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_FOUR_OF_A_KIND);

        assert_eq!(HandType::FourOfAKind, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_FULL_HOUSE: &str = "23332 3";

    #[test]
    fn hand_with_bid_hand_type_should_return_full_house() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_FULL_HOUSE);

        assert_eq!(HandType::FullHouse, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_THREE_OF_A_KIND: &str = "TTT98 4";

    #[test]
    fn hand_with_bid_hand_type_should_return_three_of_a_kind() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_THREE_OF_A_KIND);

        assert_eq!(HandType::ThreeOfAKind, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_TWO_PAIR: &str = "23432 5";

    #[test]
    fn hand_with_bid_hand_type_should_return_two_pair() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_TWO_PAIR);

        assert_eq!(HandType::TwoPair, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_ONE_PAIR: &str = "A23A4 6";

    #[test]
    fn hand_with_bid_hand_type_should_return_one_pair() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_ONE_PAIR);

        assert_eq!(HandType::OnePair, hand_with_bid.hand_type());
    }

    const EXAMPLE_HAND_HIGH_CARD: &str = "23456 7";

    #[test]
    fn hand_with_bid_hand_type_should_return_high_card() {
        let hand_with_bid = HandWithBid::from_str(EXAMPLE_HAND_HIGH_CARD);

        assert_eq!(HandType::HighCard, hand_with_bid.hand_type());
    }

    const EXAMPLE: &str = r"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn sort_hands_by_strength() {
        let mut hands: Vec<HandWithBid> =
            EXAMPLE.lines().map(|l| HandWithBid::from_str(l)).collect();

        hands.sort_by(|hand_a, hand_b| {
            let hand_a_strength = hand_a.hand_type().strength();
            let hand_b_strength = hand_b.hand_type().strength();

            if hand_a_strength < hand_b_strength {
                Ordering::Less
            } else if hand_a_strength == hand_b_strength {
                for (index, card_hand_a) in hand_a.hand.chars().enumerate() {
                    let card_hand_a_strength = STRENGTH_OF_CARDS.iter().position(|c| *c == card_hand_a).unwrap();

                    let card_hand_b = hand_b.hand.chars().nth(index).unwrap();
                    let card_hand_b_strength = STRENGTH_OF_CARDS.iter().position(|c| *c == card_hand_b).unwrap();

                    if card_hand_a_strength < card_hand_b_strength {
                        return Ordering::Less
                    }
                    if card_hand_a_strength > card_hand_b_strength {
                        return Ordering::Greater
                    }
                }

                Ordering::Equal
            } else {
                Ordering::Greater
            }
        });


        assert_eq!(
            vec![
                HandWithBid { hand: "32T3K".to_string(), bid: 765 },
                HandWithBid { hand: "KTJJT".to_string(), bid: 220 },
                HandWithBid { hand: "KK677".to_string(), bid: 28 },
                HandWithBid { hand: "T55J5".to_string(), bid: 684 },
                HandWithBid { hand: "QQQJA".to_string(), bid: 483 },
            ],
            hands
        );
    }
}

