use num::integer::lcm;

const EXAMPLE: &str = r"LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

fn main() {
    // let steps = number_of_steps_to_reach_zzz(EXAMPLE);

    let input = std::fs::read_to_string("input.txt").unwrap();
    let (movements, network) = parse_input(&input);

    let steps_zzz = number_of_steps_to_reach_zzz(movements.clone(), network.clone());
    println!("Steps required to reach ZZZ: {}", steps_zzz);

    let steps_to_reach_all_z_nodes = number_of_steps_to_reach_all_z_nodes(movements, network);
    println!("Steps required to reach all Z nodes: {}", steps_to_reach_all_z_nodes);
}

fn parse_input(maps_text: &str) -> (Vec<char>, Network) {
    let mut lines = maps_text.lines();

    let movements: Vec<char> = lines.next().unwrap().chars().collect();

    // skip the empty line
    lines.next().unwrap();

    let mut nodes_text: Vec<String> = vec![]; 

    while let Some(line) = lines.next() {
        nodes_text.push(line.to_string());
    }

    (movements, Network::from_str(&nodes_text.join("\n")))
}

fn number_of_steps_to_reach_all_z_nodes(movements: Vec<char>, network: Network) -> i64 {
    let all_starting_nodes = network.all_node_ids_ending_with('A');

    let mut sub_networks: Vec<_> = all_starting_nodes.iter()
        .map(|starting_node| {
            let mut sub_net = network.clone();
            sub_net.reset(starting_node.clone(), MovementStopCondition::OnNodeSuffix('Z'));
            sub_net
        })
        .collect();

    let mut steps_required_for_sub_networks: Vec<i64> = vec![];

    for sub_network in sub_networks.iter_mut() {
        steps_required_for_sub_networks.push(
            number_of_steps_to_reach_target(movements.clone(), sub_network.clone()));
    }

    let steps_required = steps_required_for_sub_networks.iter()
        .fold(steps_required_for_sub_networks[0], |acc, steps| {
            lcm(acc, *steps)
        });

    steps_required
}

fn number_of_steps_to_reach_zzz(movements: Vec<char>, mut network: Network) -> i64 {
    network.reset("AAA".to_string(), MovementStopCondition::OnNodeId("ZZZ".to_string()));

    number_of_steps_to_reach_target(movements, network)
}

fn number_of_steps_to_reach_target(
    movements: Vec<char>, mut network: Network,
) -> i64 {
    let mut steps_required: i64 = 0;

    'movement_loop: loop {
        for movement in movements.iter() {
            match network.advance(*movement) {
                MovementResult::AtTargetPosition => break 'movement_loop,
                MovementResult::HasMoved => steps_required += 1,
            }
        }
    }

    steps_required
}

#[derive(Debug, Eq, PartialEq, Clone)]
enum MovementStopCondition {
    OnNodeId(String),
    OnNodeSuffix(char),
}

#[derive(Debug, Eq, PartialEq, Clone)]
enum MovementResult {
    HasMoved,
    AtTargetPosition,
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct Network {
    nodes: Vec<NetworkNode>,
    index: usize,
    movement_stop_condition: Option<MovementStopCondition>,
}

impl Network {
    pub fn from_str(text: &str) -> Self {
        let mut nodes: Vec<NetworkNode> = vec![];

        for line in text.lines() {
            nodes.push(NetworkNode::from_str(line));
        }

        Self {
            nodes,
            index: 0,
            movement_stop_condition: None,
        }
    }

    pub fn reset(
        &mut self, starting_node_id: String, movement_stop_condition: MovementStopCondition
    ) {
        self.index = self.nodes.iter().position(|n| n.id == starting_node_id).unwrap();
        self.movement_stop_condition = Some(movement_stop_condition);
    }

    pub fn advance(&mut self, movement: char) -> MovementResult {
        let current_node = self.nodes.get(self.index).unwrap();

        if let Some(movement_stop_condition) = &self.movement_stop_condition {
            match movement_stop_condition {
                MovementStopCondition::OnNodeId(stop_node_id) =>
                    if &current_node.id == stop_node_id {
                        return MovementResult::AtTargetPosition;
                    }

                MovementStopCondition::OnNodeSuffix(c) =>
                    if current_node.id.ends_with(*c) {
                        return MovementResult::AtTargetPosition;
                    }
            }
        }

        let target_node = current_node.target_by_movement(movement);
        self.index = self.nodes.iter().position(|n| n.id == target_node).unwrap();

        MovementResult::HasMoved
    }

    pub fn all_node_ids_ending_with(&self, c: char) -> Vec<String> {
        self.nodes.iter()
            .filter(|n| n.id.ends_with(c))
            .map(|n| n.id.clone())
            .collect()
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
struct NetworkNode {
    id: String,
    left: String,
    right: String,
}

impl NetworkNode {
    pub fn from_str(text: &str) -> Self {
        let id = text[ .. 3].to_string();
        let left = text[ 7 .. 10 ].to_string();
        let right = text[ 12 .. 15 ].to_string();

        Self {
            id,
            left,
            right,
        }
    }

    pub fn target_by_movement(&self, movement: char) -> &str {
        if movement == 'L' {
            &self.left
        } else {
            &self.right
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::NetworkNode;

    const EXAMPLE_NODE: &str = "AAA = (BBB, CCC)";

    #[test]
    fn network_node_from_str_should_parse_example_node() {
        let network_node = NetworkNode::from_str(EXAMPLE_NODE);

        assert_eq!(
            NetworkNode {
                id: "AAA".to_string(),
                left: "BBB".to_string(),
                right: "CCC".to_string(),
            },
            network_node
        );
    }
}

