use std::slice::Iter;

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Seeds {
    seeds: Vec<u64>,
}

impl Seeds {
    pub fn new(seeds: &[u64]) -> Self {
        Self { seeds: Vec::from(seeds) }
    }

    pub fn from_str(seeds_line: &str) -> Self {
        let seeds = if let Some(index) = seeds_line.find(':') {
                seeds_line[ index + 1 .. ]
                    .split_whitespace()
                    .map(|seed| seed.parse::<u64>().unwrap())
                    .collect()
            } else {
                vec![]
            };

        Self { seeds }
    }

    pub fn from_str_range(seeds_line: &str) -> Self {
        let seed_ranges = if let Some(index) = seeds_line.find(':') {
                seeds_line[ index + 1 .. ]
                    .split_whitespace()
                    .map(|seed| seed.parse::<u64>().unwrap())
                    .collect()
            } else {
                vec![]
            };

        let mut seeds: Vec<u64> = vec![];

        for seed_range in seed_ranges.chunks(2) {
            (seed_range[0] .. seed_range[0] + seed_range[1]).for_each(|seed| seeds.push(seed));
        }

        Self { seeds }
    }

    pub fn iter(&self) -> Iter<u64> {
        self.seeds.iter()
    }

    pub fn len(&self) -> usize {
        self.seeds.len()
    }
}

#[cfg(test)]
mod tests {
    use crate::seeds::Seeds;

    const SEEDS_EXAMPLE: &str = "seeds: 79 14 55 13";

    #[test]
    fn from_str_should_return_79_14_55_13() {
        let seeds = Seeds::from_str(SEEDS_EXAMPLE);

        assert_eq!(
            vec![79, 14, 55, 13],
            seeds.seeds
        );
    }

    #[test]
    fn from_str_range_should_return_27() {
        let seeds = Seeds::from_str_range(SEEDS_EXAMPLE);

        assert_eq!(27, seeds.iter().len());
    }

    #[test]
    fn from_str_range_should_return_all_seeds() {
        let seeds = Seeds::from_str_range(SEEDS_EXAMPLE);

        assert_eq!(
            vec![
                79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
                55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67,
            ],
            seeds.seeds
        );
    }
}

