use once_cell::sync::Lazy;
use regex::Regex;

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct MappingHeading {
    source: String,
    destination: String,
}

impl MappingHeading {
    pub fn from_str(mapping_heading_text: &str) -> Self {
        Self::from(mapping_heading_text.to_string())
    }
    
    pub fn new(source: &str, destination: &str) -> Self {
        Self {
            source: source.to_string(),
            destination: destination.to_string(),
        }
    }

    pub fn destination(&self) -> &str {
        self.destination.as_str()
    }
}

static MAPPING_HEADING_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"^([a-z]+)\-to\-([a-z]+)\smap:$").unwrap()
});

impl From<String> for MappingHeading {
    fn from(mapping_heading_text: String) -> Self {
        let mut mapping_heading = Self::default();

        if let Some(captures) = MAPPING_HEADING_REGEX.captures(&mapping_heading_text) {
            if let Some(source_capture) = captures.get(1) {
                mapping_heading.source = source_capture.as_str().to_string();
            }

            if let Some(destination_capture) = captures.get(2) {
                mapping_heading.destination = destination_capture.as_str().to_string();
            }
        }

        mapping_heading
    }
}

#[cfg(test)]
mod tests {
    use crate::mapping_heading::MappingHeading;

    const SEED_TO_SOIL_MAP_HEADING: &str = "seed-to-soil map:";

    #[test]
    fn from_string_should_return_source_and_destination() {
        let mapping_heading = MappingHeading::from(SEED_TO_SOIL_MAP_HEADING.to_string());

        assert_eq!("seed", mapping_heading.source);
        assert_eq!("soil", mapping_heading.destination);
    }
}

