use crate::{mapping_heading::MappingHeading, mapping_range::MappingRange};

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Mapping {
    heading: MappingHeading,
    ranges: Vec<MappingRange>,
}

impl Mapping {
    pub fn from_str(mapping_text: &str) -> Self {
        let mut mapping = Self::default();

        let mut lines = mapping_text.lines();

        if let Some(heading_text) = lines.next() {
            mapping.heading = MappingHeading::from_str(heading_text);
        }

        while let Some(mapping_range_line) = lines.next() {
            mapping.ranges.push(MappingRange::from_str(mapping_range_line));
        }

        mapping
    }

    // TODO: Try to work with a range instead of single seed numbers. Might be that safes some
    // memory. This still does not answer, why the memory is constantly increasing.
    // Maybe I should replace some of the implicit clones with borrows.
    pub fn destination(&self, source: &u64) -> u64 {
        for mapping_range in self.ranges.iter() {
            if let Some(destination) = mapping_range.destination(source) {
                return destination;
            }
        }

        *source
    }

    pub fn heading(&self) -> &MappingHeading {
        &self.heading
    }
}

#[cfg(test)]
mod tests {
    use crate::{mapping::Mapping, mapping_range::MappingRange, mapping_heading::MappingHeading};

    const SEED_TO_SOIL_MAPPING: &str = r"seed-to-soil map:
50 98 2
52 50 48";

    #[test]
    fn from_str_should_parse_heading_and_all_mappings() {
        let mapping = Mapping::from_str(SEED_TO_SOIL_MAPPING);

        assert_eq!(
            MappingHeading::new("seed", "soil"),
            mapping.heading,
        );
        assert_eq!(
            vec![
                MappingRange::new(50, 98, 2),
                MappingRange::new(52, 50, 48),
            ],
            mapping.ranges,
        );
    }

    #[test]
    fn destination_should_return_soil_81_for_seed_79() {
        let mapping = Mapping::from_str(SEED_TO_SOIL_MAPPING);
        assert_eq!(81, mapping.destination(&79));
    }

    #[test]
    fn destination_should_return_soil_14_for_seed_14() {
        let mapping = Mapping::from_str(SEED_TO_SOIL_MAPPING);
        assert_eq!(14, mapping.destination(&14));
    }

    #[test]
    fn destination_should_return_soil_57_for_seed_55() {
        let mapping = Mapping::from_str(SEED_TO_SOIL_MAPPING);
        assert_eq!(57, mapping.destination(&55));
    }

    #[test]
    fn destination_should_return_soil_13_for_seed_13() {
        let mapping = Mapping::from_str(SEED_TO_SOIL_MAPPING);
        assert_eq!(13, mapping.destination(&13));
    }
}

