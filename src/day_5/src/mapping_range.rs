#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct MappingRange {
    destination_range_start: u64,
    source_range_start: u64,
    range_length: u64,
}

impl From<String> for MappingRange {
    fn from(mapping_range_text: String) -> Self {
        let numbers: Vec<_> = mapping_range_text
            .split_whitespace()
            .map(|n| n.parse::<u64>().unwrap())
            .collect();

        Self::new(numbers[0], numbers[1], numbers[2])
    }
}

impl MappingRange {
    pub fn new(destination_range_start: u64, source_range_start: u64, range_length: u64) -> Self {
        Self {
            destination_range_start,
            source_range_start,
            range_length,
        }
    }

    pub fn from_str(mapping_range_text: &str) -> Self {
        Self::from(mapping_range_text.to_string())
    }

    pub fn destination(&self, source: &u64) -> Option<u64> {
        if *source >= self.source_range_start
            && *source < (self.source_range_start + self.range_length)
        {
            let offset = source - self.source_range_start;

            Some(self.destination_range_start + offset)
        } else {
            None
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::mapping_range::MappingRange;

    const SEED_TO_SOIL_MAP_MAPPING_RANGE_1: &str = "50 98 2";

    #[test]
    fn from_string_should_return_ranges() {
        let mapping_range = MappingRange::from(SEED_TO_SOIL_MAP_MAPPING_RANGE_1.to_string());

        assert_eq!(50, mapping_range.destination_range_start);
        assert_eq!(98, mapping_range.source_range_start);
        assert_eq!(2, mapping_range.range_length);
    }

    #[test]
    fn destination_should_return_50_for_98() {
        let mapping_range = MappingRange::from_str(SEED_TO_SOIL_MAP_MAPPING_RANGE_1);

        assert_eq!(Some(50), mapping_range.destination(&98));
    }

    #[test]
    fn destination_should_return_51_for_99() {
        let mapping_range = MappingRange::from_str(SEED_TO_SOIL_MAP_MAPPING_RANGE_1);

        assert_eq!(Some(51), mapping_range.destination(&99));
    }

    #[test]
    fn destination_should_return_none_for_100() {
        let mapping_range = MappingRange::from_str(SEED_TO_SOIL_MAP_MAPPING_RANGE_1);

        assert_eq!(None, mapping_range.destination(&100));
    }

    #[test]
    fn destination_should_return_none_for_97() {
        let mapping_range = MappingRange::from_str(SEED_TO_SOIL_MAP_MAPPING_RANGE_1);

        assert_eq!(None, mapping_range.destination(&97));
    }
}

