mod almanac;
mod mapping;
mod mapping_heading;
mod mapping_range;
mod seed_range;
mod seeds;

use std::time::Instant;

use seeds::Seeds;

use crate::{almanac::Almanac, seed_range::parse_seed_ranges};

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();
    let almanac = parse_almanac(&input);

    solve_part_1(&almanac, &input);

    println!();

    solve_part_2(&almanac, &input);
}

fn solve_part_1(almanac: &Almanac, input_text: &str) {
    let now = Instant::now();

    let seeds = parse_seeds(input_text).unwrap();
    let locations = almanac.locations(&seeds);
    let min = locations.iter().min().unwrap();

    println!();
    println!("part 1: lowest location number is {} (took: {:.2?})", min, now.elapsed());
}

fn parse_seeds(input_text: &str) -> Option<Seeds> {
    if let Some(seeds_line) = input_text.lines().next() {
        let now = Instant::now();

        let seeds = Seeds::from_str(seeds_line);

        println!("seed parsing took: {:.2?}", now.elapsed());
        println!("there are {} seeds to process", seeds.len());

        Some(seeds)
    } else {
        None
    }
}

fn solve_part_2(almanac: &Almanac, input_text: &str) {
    let now = Instant::now();

    let seed_ranges = parse_seed_ranges(input_text).unwrap();
    let locations = almanac.locations_with_ranges(&seed_ranges);
    let min = locations.iter().min().unwrap();

    println!();
    println!("part 2: lowest location number is {} (took: {:.2?})", min, now.elapsed());
}

fn parse_almanac(input_text: &str) -> Almanac {
    let index = input_text.find("\n\n").unwrap() + 2;

    Almanac::from_str(&input_text[ index .. ])
}

