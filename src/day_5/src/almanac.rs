use crate::{mapping::Mapping, seeds::Seeds, seed_range::SeedRange};

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Almanac {
    mappings: Vec<Mapping>,
}

impl Almanac {
    pub fn from_str(almanac_text: &str) -> Self {
        let mut almanac = Self::default();

        let mut mapping_chunk = String::from("");

        for line in almanac_text.lines() {
            if line.is_empty() {
                almanac.mappings.push(Mapping::from_str(&mapping_chunk));
                mapping_chunk.clear();
            } else {
                mapping_chunk.push_str(&format!("{}\n", line));
            }
        }

        almanac.mappings.push(Mapping::from_str(&mapping_chunk));

        almanac
    }

    pub fn locations(&self, seeds: &Seeds) -> Vec<u64> {
        let mut locations: Vec<u64> = Vec::with_capacity(seeds.len());

        for seed in seeds.iter() {
            let mut current_destination = *seed;

            for mapping in self.mappings.iter() {
                current_destination = mapping.destination(&current_destination);
            }

            locations.push(current_destination);
        }

        locations
    }

    pub fn locations_with_ranges(&self, seed_ranges: &[SeedRange]) -> Vec<u64> {
        let total_len: u64 = seed_ranges.iter().map(|seed_range| seed_range.len()).sum();
        let mut locations: Vec<u64> = Vec::with_capacity(total_len as usize);

        for seed_range in seed_ranges.iter() {
            for seed in seed_range.range() {
                let mut current_destination = seed;

                for mapping in self.mappings.iter() {
                    current_destination = mapping.destination(&current_destination);
                }

                locations.push(current_destination);
            }
        }

        locations
    }
}

#[cfg(test)]
mod tests {
    use crate::{Almanac, mapping_heading::MappingHeading}; 

    const ALMANAC: &str = r"seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn almanac_should_parse_all_maps() {
        let almanac = Almanac::from_str(ALMANAC);

        let mapping_headings: Vec<_> = almanac.mappings.iter()
            .map(|mapping| mapping.heading().clone())
            .collect();

        assert_eq!(
            vec![
                MappingHeading::new("seed", "soil"),
                MappingHeading::new("soil", "fertilizer"),
                MappingHeading::new("fertilizer", "water"),
                MappingHeading::new("water", "light"),
                MappingHeading::new("light", "temperature"),
                MappingHeading::new("temperature", "humidity"),
                MappingHeading::new("humidity", "location"),
            ],
            mapping_headings
        );
    }
}

