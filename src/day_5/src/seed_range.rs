use std::{ops::Range, time::Instant};

pub fn parse_seed_ranges(input_text: &str) -> Option<Vec<SeedRange>> {
    if let Some(seeds_line) = input_text.lines().next() {
        let now = Instant::now();

        let tokens = if let Some(index) = seeds_line.find(':') {
                seeds_line[ index + 1 .. ]
                    .split_whitespace()
                    .map(|seed| seed.parse::<u64>().unwrap())
                    .collect()
            } else {
                vec![]
            };

        let mut seed_ranges: Vec<SeedRange> = vec![];

        for seed_range_chunk in tokens.chunks(2) {
            seed_ranges.push(
                SeedRange::from_str(&format!("{} {}", seed_range_chunk[0], seed_range_chunk[1])));
        }

        println!("seed parsing took: {:.2?}", now.elapsed());

        let total_len: u64 = seed_ranges.iter().map(|seed_range| seed_range.len()).sum();
        println!("there are {} ranges with a total of {} seeds to process",
            seed_ranges.len(), total_len);

        Some(seed_ranges)
    } else {
        None
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SeedRange {
    start: u64,
    len: u64,
}

impl SeedRange {
    pub fn new(start: u64, len: u64) -> Self {
        Self { 
            start,
            len,
        }
    }

    pub fn from_str(seed_range_text: &str) -> Self {
        let numbers: Vec<_> = seed_range_text.split_whitespace()
            .map(|seed| seed.parse::<u64>().unwrap())
            .collect();

        SeedRange::new(numbers[0], numbers[1])
    }

    pub fn in_range(&self, seed: u64) -> bool {
        seed >= self.start && seed < self.start + self.len
    }

    pub fn len(&self) -> u64 {
        self.len
    }

    pub fn range(&self) -> Range<u64> {
        self.start .. self.start + self.len - 1
    }
}

#[cfg(test)]
mod tests {
    use crate::seed_range::SeedRange;

    use super::parse_seed_ranges;

    const SEED_RANGE: &str = "79 14";

    #[test]
    fn from_str_should_parse_range_from_79_to_92() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert_eq!(79, seed_range.start);
        assert_eq!(14, seed_range.len);
    }

    #[test]
    fn in_range_should_return_false_for_78() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert!(!seed_range.in_range(78));
    }

    #[test]
    fn in_range_should_return_false_for_93() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert!(!seed_range.in_range(93));
    }

    #[test]
    fn in_range_should_return_true_for_83() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert!(seed_range.in_range(83));
    }

    #[test]
    fn in_range_should_return_true_for_79() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert!(seed_range.in_range(79));
    }

    #[test]
    fn in_range_should_return_true_for_92() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert!(seed_range.in_range(92));
    }

    const SEED_RANGES: &str = r"seeds: 79 14 55 13";

    #[test]
    fn parse_seed_ranges_should_return_79_14_and_55_13() {
        let seed_ranges = parse_seed_ranges(SEED_RANGES).unwrap();

        assert_eq!(vec![SeedRange::new(79, 14), SeedRange::new(55, 13)], seed_ranges);
    }

    #[test]
    fn range_should_return_range_from_79_to_92() {
        let seed_range = SeedRange::from_str(SEED_RANGE);

        assert_eq!(79 .. 92, seed_range.range());
    }
}

