use once_cell::sync::Lazy;

fn main() {
    let calibration_document = std::fs::read_to_string("input.txt").unwrap();

    println!();
    println!("Sum of calibration values is: {} (part 1)",
        sum_of_calibration_values(&calibration_document));

    println!();
    println!("Sum of calibration values is: {} (part 2)",
        sum_of_calibration_values_with_words(&calibration_document));
}

fn sum_of_calibration_values(calibration_document: &str) -> u64 {
    let mut sum_of_calibration_values: u64 = 0;

    for line in calibration_document.lines() {
        let calibration_value = extract_calibration_value(line);

        sum_of_calibration_values += calibration_value as u64;
    }

    sum_of_calibration_values
}

fn extract_calibration_value(document_line: &str) -> u8 {
    let (first_digit, last_digit) = extract_first_and_last_digit(document_line);

    format!("{}{}", first_digit.1, last_digit.1).parse::<u8>().unwrap()
}

fn sum_of_calibration_values_with_words(calibration_document: &str) -> u64 {
    let mut sum_of_calibration_values: u64 = 0;

    for line in calibration_document.lines() {
        let calibration_value = extract_calibration_value_with_words(line);

        sum_of_calibration_values += calibration_value as u64;
    }

    sum_of_calibration_values
}

static WORD_TO_DIGIT: Lazy<Vec<(String, u8)>> = Lazy::new(|| {
    vec![
        ("one".to_string(), 1),
        ("two".to_string(), 2),
        ("three".to_string(), 3),
        ("four".to_string(), 4),
        ("five".to_string(), 5),
        ("six".to_string(), 6),
        ("seven".to_string(), 7),
        ("eight".to_string(), 8),
        ("nine".to_string(), 9),
    ]
});

fn extract_calibration_value_with_words(document_line: &str) -> u8 {
    let mut occurences_of_words: Vec<(usize, String)> = vec![];

    for (word, _) in WORD_TO_DIGIT.iter() {
        for (index, substring) in document_line.match_indices(word) {
            occurences_of_words.push((index, substring.to_string()));
        }
    }

    // if this does not have any digits, just return the words
    if document_line.chars().find(|c| c.is_digit(10)).is_none() {
        let first_word_index = occurences_of_words.iter().map(|(index, _)| index).min().unwrap();
        let first_word = occurences_of_words.iter()
            .find(|(index, _)| first_word_index == index).map(|(_, word)| word).unwrap();
        let first = WORD_TO_DIGIT.iter()
            .find(|(word,_)| word == first_word).map(|(_,d)| *d).unwrap();

        let last_word_index = occurences_of_words.iter().map(|(index, _)| index).max().unwrap();
        let last_word = occurences_of_words.iter()
            .find(|(index, _)| last_word_index == index).map(|(_, word)| word).unwrap();
        let last = WORD_TO_DIGIT.iter()
            .find(|(word,_)| word == last_word).map(|(_,d)| *d).unwrap();

        return format!("{}{}", first, last).parse::<u8>().unwrap();
    }

    let (first_digit, last_digit) = extract_first_and_last_digit(document_line);
    let first;
    let last;

    // check if there are words with a smaller index then the `first_digit`
    let first_words: Vec<_> = occurences_of_words.iter()
        .filter(|(index, _)| *index < first_digit.0).collect();
    if !first_words.is_empty() {
        let first_word_index = first_words.iter().map(|(index, _)| index).min().unwrap();
        let first_word = first_words.iter()
            .find(|(index, _)| first_word_index == index).map(|(_, word)| word).unwrap();

        first = WORD_TO_DIGIT.iter().find(|(word,_)| word == first_word).map(|(_,d)| *d).unwrap();
    } else {
        first = first_digit.1;
    }

    // check if there are words with a greater index then the `last_digit`
    let last_words: Vec<_> = occurences_of_words.iter()
        .filter(|(index, _)| *index > last_digit.0).collect();
    if !last_words.is_empty() {
        let last_word_index = last_words.iter().map(|(index, _)| index).max().unwrap();
        let last_word = last_words.iter()
            .find(|(index, _)| last_word_index == index).map(|(_, word)| word).unwrap();

        last = WORD_TO_DIGIT.iter().find(|(word,_)| word == last_word).map(|(_,d)| *d).unwrap();
    } else {
        last = last_digit.1;
    }

    format!("{}{}", first, last).parse::<u8>().unwrap()
}

fn extract_first_and_last_digit(document_line: &str) -> ((usize, u8), (usize, u8)) {
    let mut first_digit: Option<(usize, u8)> = None;
    let mut last_digit: Option<(usize, u8)> = None;

    for (index, c) in document_line.chars().enumerate() {
        if c.is_digit(10) {
            let digit = c.to_string().parse::<u8>().unwrap();

            if first_digit.is_none() {
                first_digit = Some((index, digit));
            }

            last_digit = Some((index, digit));
        }
    }

    (first_digit.unwrap(), last_digit.unwrap())
}

#[cfg(test)]
mod tests {
    use crate::{sum_of_calibration_values, extract_calibration_value, extract_calibration_value_with_words, sum_of_calibration_values_with_words};

    const CALIB_DOC_LINE: &str = "1abc2";

    #[test]
    fn extract_calibration_value_should_extract_12_from_1abc2() {
        assert_eq!(12, extract_calibration_value(CALIB_DOC_LINE));
    }

    #[test]
    fn extract_calibration_value_should_extract_33_from_pplbxsvmqfjjvfhnn3() {
        assert_eq!(33, extract_calibration_value("pplbxsvmqfjjvfhnn3"));
    }

    #[test]
    fn extract_calibration_value_should_extract_33_from_k3() {
        assert_eq!(33, extract_calibration_value("k3"));
    }

    #[test]
    fn extract_calibration_value_should_extract_44_from_4d() {
        assert_eq!(44, extract_calibration_value("4d"));
    }

const CALIBRATION_DOCUMENT: &str = r"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";

    #[test]
    fn sum_of_calibration_values_from_document_should_produce_142() {
        assert_eq!(142, sum_of_calibration_values(CALIBRATION_DOCUMENT));
    }

    const DOCUMENT_LINE_WITH_WORDS_1: &str = r"two1nine";

    #[test]
    fn extract_calibration_value_with_words_should_be_29_for_words_1() {
        assert_eq!(29, extract_calibration_value_with_words(DOCUMENT_LINE_WITH_WORDS_1));
    }

    const DOCUMENT_LINE_WITH_WORDS_2: &str = r"eightwothree";

    #[test]
    fn extract_calibration_value_with_words_should_be_83_for_words_2() {
        assert_eq!(83, extract_calibration_value_with_words(DOCUMENT_LINE_WITH_WORDS_2));
    }

    const DOCUMENT_LINE_WITH_WORDS_3: &str = r"abcone2threexyz";

    #[test]
    fn extract_calibration_value_with_words_should_be_13_for_words_3() {
        assert_eq!(13, extract_calibration_value_with_words(DOCUMENT_LINE_WITH_WORDS_3));
    }

    const DOCUMENT_LINE_WITH_WORDS_6: &str = r"zoneight234";

    #[test]
    fn extract_calibration_value_with_words_should_be_14_for_words_6() {
        assert_eq!(14, extract_calibration_value_with_words(DOCUMENT_LINE_WITH_WORDS_6));
    }

const CALIBRATION_DOCUMENT_WITH_WORDS: &str = r"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

    #[test]
    fn sum_of_calibration_values_with_words_from_document_should_produce_281() {
        assert_eq!(281, sum_of_calibration_values_with_words(CALIBRATION_DOCUMENT_WITH_WORDS));
    }
}

