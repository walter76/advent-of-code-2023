mod app;
mod app_state;

use aoc_core::text_map::TextMap;

const CONTRAPTION_LAYOUT: &str = r".|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....";

const CONTRAPTION_LOOP_LAYOUT: &str = r"....\.....
..........
..........
....|...\.
..........
..........
....\.../.
..........
..........
..........";

fn main() {
    // app::run(CONTRAPTION_LAYOUT).unwrap();

    let input = std::fs::read_to_string("input.txt").unwrap();

    let contraption_layout = TextMap::from_str(CONTRAPTION_LOOP_LAYOUT);
    let mut energizer = Energizer::new(contraption_layout.clone());

    println!("{}", contraption_layout);
    println!();

    let energized = energizer.energize();

    println!("{}", energized);
    println!();

    let tiles_energized = energized.iter().filter(|c| **c == '#').count();

    println!("{}", tiles_energized);
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y, }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum BeamState {
    PointAdded(Point),
    CreateBeam(Point, Direction),
    BeamDied,
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Beam {
    id: usize,
    visited: Vec<(Direction, Point)>,
    current_direction: Direction,
}

impl Beam {
    pub fn new(id: usize, start_point: Point, direction: Direction) -> Self {
        Self {
            id,
            visited: vec![ (direction.clone(), start_point) ],
            current_direction: direction,
        }
    }

    pub fn last(&self) -> (Direction, Point) {
        self.visited.last().unwrap().clone()
    }

    pub fn next(&mut self, contraption_layout: &TextMap) -> BeamState {
        let mut point = self.last().1.clone();

        match self.current_direction {
            Direction::North => {
                if point.y > 0 {
                    point.y -= 1;

                    self.add_point(contraption_layout, point).unwrap()
                } else {
                    BeamState::BeamDied
                }
            }

            Direction::East => {
                if point.x < contraption_layout.width() - 1 {
                    point.x += 1;

                    self.add_point(contraption_layout, point).unwrap()
                } else {
                    BeamState::BeamDied
                }
            }

            Direction::South => {
                if point.y < contraption_layout.height() - 1 {
                    point.y += 1;

                    self.add_point(contraption_layout, point).unwrap()
                } else {
                    BeamState::BeamDied
                }
            }

            Direction::West => {
                if point.x > 0 {
                    point.x -= 1;

                    self.add_point(contraption_layout, point).unwrap()
                } else {
                    BeamState::BeamDied
                }
            }
        }
    }

    fn add_point(&mut self, contraption_layout: &TextMap, point: Point) -> Option<BeamState> {
        // println!("({:?}, {:?}): {:#?}", self.current_direction, point, self.visited);

        match contraption_layout.char_at(point.x, point.y) {
            '.' => {
                // if this beam has a loop, the beam stops
                if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                    println!("Beam {}: loop detected -> beam dies", self.id);

                    Some(BeamState::BeamDied)
                } else {
                    self.visited.push((self.current_direction.clone(), point.clone()));

                    Some(BeamState::PointAdded(point))
                }
            }
            '\\' => {
                match self.current_direction {
                    Direction::South => self.current_direction = Direction::East,
                    Direction::East => self.current_direction = Direction::South,
                    Direction::North => self.current_direction = Direction::West,
                    Direction::West => self.current_direction = Direction::North,
                }

                // if this beam has a loop, the beam stops
                if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                    println!("Beam {}: loop detected -> beam dies", self.id);

                    Some(BeamState::BeamDied)
                } else {
                    self.visited.push((self.current_direction.clone(), point.clone()));

                    Some(BeamState::PointAdded(point))
                }
            }
            '/' => {
                match self.current_direction {
                    Direction::South => self.current_direction = Direction::West,
                    Direction::East => self.current_direction = Direction::North,
                    Direction::North => self.current_direction = Direction::East,
                    Direction::West => self.current_direction = Direction::South,
                }

                // if this beam has a loop, the beam stops
                if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                    println!("Beam {}: loop detected -> beam dies", self.id);

                    Some(BeamState::BeamDied)
                } else {
                    self.visited.push((self.current_direction.clone(), point.clone()));

                    Some(BeamState::PointAdded(point))
                }
            }
            '|' => match self.current_direction {
                Direction::South | Direction::North => {
                    // if this beam has a loop, the beam stops
                    if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                        println!("Beam {}: loop detected -> beam dies", self.id);

                        Some(BeamState::BeamDied)
                    } else {
                        self.visited.push((self.current_direction.clone(), point.clone()));

                        Some(BeamState::PointAdded(point))
                    }
                }
                Direction::East | Direction::West => {
                    // if this beam has a loop, the beam stops
                    if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                        println!("Beam {}: loop detected -> beam dies", self.id);

                        Some(BeamState::BeamDied)
                    } else {
                        self.current_direction = Direction::North;
                        self.visited.push((self.current_direction.clone(), point.clone()));

                        Some(BeamState::CreateBeam(point, Direction::South))
                    }
                }
            }
            '-' => match self.current_direction {
                Direction::South | Direction::North => {
                    // if this beam has a loop, the beam stops
                    if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                        println!("Beam {}: loop detected -> beam dies", self.id);

                        Some(BeamState::BeamDied)
                    } else {
                        self.current_direction = Direction::West;
                        self.visited.push((self.current_direction.clone(), point.clone()));

                        Some(BeamState::CreateBeam(point, Direction::East))
                    }
                }
                Direction::East | Direction::West  => {
                    // if this beam has a loop, the beam stops
                    if self.visited.contains(&(self.current_direction.clone(), point.clone())) {
                        println!("Beam {}: loop detected -> beam dies", self.id);

                        Some(BeamState::BeamDied)
                    } else {
                        self.visited.push((self.current_direction.clone(), point.clone()));

                        Some(BeamState::PointAdded(point))
                    }
                }
            }
            _ => None
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Energizer {
    contraption_layout: TextMap,
    active_beams: Vec<Beam>,
    points_energized: Vec<Point>,
}

impl Energizer {
    pub fn new(contraption_layout: TextMap) -> Self {
        Self {
            contraption_layout,
            active_beams: vec![],
            points_energized: vec![],
        }
    }

    pub fn energize(&mut self) -> TextMap {
        let mut energized = TextMap::new(
            self.contraption_layout.width(), self.contraption_layout.height(), vec![]);

        energized.fill('.');

        let mut next_beam_id: usize = 0;
        let initial_beam = Beam::new(next_beam_id, Point::new(0, 0), Direction::East);
        next_beam_id += 1;

        self.active_beams.push(initial_beam);

        energized.set_char_at(0, 0, '#');

        loop {
            let mut beams_that_have_died: Vec<usize> = vec![];
            let mut beams_created: Vec<Beam> = vec![];

            for beam in self.active_beams.iter_mut() {
                match beam.next(&self.contraption_layout) {
                    BeamState::BeamDied => {
                        println!("Beam {} died", beam.id);
                        beams_that_have_died.push(beam.id);
                    }
                    BeamState::PointAdded(p) => {
                        if energized.char_at(p.x, p.y) != '#' {
                            println!("Beam {} is energizing x={}, y={}", beam.id, p.x, p.y);
                            energized.set_char_at(p.x, p.y, '#');
                        }
                    }
                    BeamState::CreateBeam(p, d) => {
                        println!("Beam {} is creating new beam {}, x={}, y={}, d={:?}", beam.id, next_beam_id, p.x, p.y, d);
                        let new_beam = Beam::new(next_beam_id, p.clone(), d);
                        next_beam_id += 1;

                        beams_created.push(new_beam);

                        if energized.char_at(p.x, p.y) != '#' {
                            println!("Beam {} is energizing x={}, y={}", beam.id, p.x, p.y);
                            energized.set_char_at(p.x, p.y, '#');
                        }
                    }
                }
            }

            while !beams_that_have_died.is_empty() {
                let beam_id = beams_that_have_died.pop().unwrap();
                let index = self.active_beams.iter().position(|b| b.id == beam_id).unwrap();
                self.active_beams.remove(index);
            }

            while !beams_created.is_empty() {
                self.active_beams.push(beams_created.pop().unwrap());
            }

            if self.active_beams.is_empty() {
                break;
            }

            if next_beam_id > 5 {
                break;
            }
        }

        energized
    }
}

#[cfg(test)]
mod tests {
    use aoc_core::text_map::TextMap;

    use crate::{Energizer, CONTRAPTION_LAYOUT};

    const STRAIGHT_BEAM_LAYOUT: &str = r"..........
..........
..........
..........
..........
..........
..........
..........
..........
..........";

    const STRAIGHT_BEAM_ENERGIZED: &str = r"##########
..........
..........
..........
..........
..........
..........
..........
..........
..........";

    #[test]
    fn energize_straight_beam() {
        let contraption_layout = TextMap::from_str(STRAIGHT_BEAM_LAYOUT);
        let mut energizer = Energizer::new(contraption_layout);

        let energized = TextMap::from_str(STRAIGHT_BEAM_ENERGIZED);

        assert_eq!(energized, energizer.energize());
    }

    const MIRROR_BEAM_LAYOUT: &str = r"....\.....
..........
..........
..........
..........
..........
..........
..........
..........
..........";

    const MIRROR_BEAM_ENERGIZED: &str = r"#####.....
....#.....
....#.....
....#.....
....#.....
....#.....
....#.....
....#.....
....#.....
....#.....";

    #[test]
    fn energize_mirror_beam() {
        let contraption_layout = TextMap::from_str(MIRROR_BEAM_LAYOUT);
        let mut energizer = Energizer::new(contraption_layout);

        let energized = TextMap::from_str(MIRROR_BEAM_ENERGIZED);

        assert_eq!(energized, energizer.energize());
    }

    const MIRROR_BEAM_LAYOUT_2: &str = r"..../.....
..........
..........
..........
..........
..........
..........
..........
..........
..........";

    const MIRROR_BEAM_ENERGIZED_2: &str = r"#####.....
..........
..........
..........
..........
..........
..........
..........
..........
..........";

    #[test]
    fn energize_mirror_beam_2() {
        let contraption_layout = TextMap::from_str(MIRROR_BEAM_LAYOUT_2);
        let mut energizer = Energizer::new(contraption_layout);

        let energized = TextMap::from_str(MIRROR_BEAM_ENERGIZED_2);

        assert_eq!(energized, energizer.energize());
    }

const CONTRAPTION_LAYOUT_ENERGIZED: &str = r"######....
.#...#....
.#...#####
.#...##...
.#...##...
.#...##...
.#..####..
########..
.#######..
.#...#.#..";

    // #[test]
    fn energize_contraption_layout() {
        let contraption_layout = TextMap::from_str(CONTRAPTION_LAYOUT);
        let mut energizer = Energizer::new(contraption_layout);

        let energized = TextMap::from_str(CONTRAPTION_LAYOUT_ENERGIZED);

        assert_eq!(energized, energizer.energize());
    }
}

