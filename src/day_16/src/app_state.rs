use aoc_core::text_map::TextMap;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct AppState {
    should_quit: bool,
    contraption_layout: TextMap,
}

impl AppState {
    pub fn from_str(input: &str) -> Self {
        let contraption_layout = TextMap::from_str(input);

        Self {
            should_quit: false,
            contraption_layout,
        }
    }

    pub fn should_quit(&self) -> bool {
        self.should_quit
    }

    pub fn on_key(&mut self, c: char) {
        if c == 'q' {
            self.should_quit = true;
        }
    }

    pub fn contraption_layout(&self) -> &TextMap {
        &self.contraption_layout
    }
}
