use std::{io::{Stdout, self}, sync::mpsc, time::{Duration, Instant}, thread};

use anyhow::Result;
use crossterm::{terminal::{enable_raw_mode, EnterAlternateScreen, disable_raw_mode, LeaveAlternateScreen}, execute, event::{EnableMouseCapture, self, KeyCode, DisableMouseCapture}};
use ratatui::{prelude::*, widgets::{Paragraph, Block, Borders}};

use crate::app_state::AppState;

const TICK_RATE: u64 = 250;     // the tick rate to be used for updating the UI

pub fn run(input: &str) -> Result<()> {
    let mut terminal = setup_terminal()?;

    let rx = setup_input_event_thread();

    // run ui loop
    let app_state = AppState::from_str(input);
    let app_result = App::with_state(app_state)?.run_ui_loop(&mut terminal, rx);

    restore_terminal(terminal)?;

    if let Err(err) = app_result {
        println!("{:?}", err);
    }

    Ok(())
}

fn setup_terminal() -> Result<Terminal<CrosstermBackend<Stdout>>> {
    enable_raw_mode()?;

    let mut stdout = io::stdout();

    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;

    Ok(Terminal::new(CrosstermBackend::new(stdout))?)
}

fn restore_terminal<B>(mut terminal: Terminal<B>) -> Result<()>
    where B: Backend, B: io::Write
{
    disable_raw_mode()?;

    execute!(terminal.backend_mut(), LeaveAlternateScreen, DisableMouseCapture)?;

    Ok(terminal.show_cursor()?)
}

enum Event<I> {
    Input(I),
    Tick,
}

fn setup_input_event_thread() -> mpsc::Receiver<Event<event::KeyEvent>> {
    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(TICK_RATE);

    // TODO: Do I need to clean-up the input event thread to not get any leaks?

    thread::spawn(move || {
        let mut last_tick = Instant::now();

        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).expect("poll next event") {
                if let event::Event::Key(key) = event::read().expect("read next event") {
                    // Using `crossterm` on Windows will lead to keyboard events sent twice. Once
                    // for key press and twice for key release. On MacOs and Linux only
                    // `KeyEventKind::Press` is sent.
                    // (see https://ratatui.rs/tutorial/counter-async-app/async-event-stream.html)
                    //
                    // Therefore, only send the key press event and ignore key release.
                    if key.kind == event::KeyEventKind::Press {
                        tx.send(Event::Input(key)).expect("send event to render thread");
                    }
                }
            }

            if last_tick.elapsed() >= tick_rate
                && tx.send(Event::Tick).is_ok()
            {
                last_tick = Instant::now();
            }
        }
    });

    rx
}

struct App {
    app_state: AppState,
}

impl App {
    pub fn with_state(app_state: AppState) -> Result<Self> {
        Ok(
            Self {
                app_state,
        })
    }

    pub fn run_ui_loop<B: Backend>(
        &mut self, terminal: &mut Terminal<B>, rx: mpsc::Receiver<Event<event::KeyEvent>>,
    ) -> Result<()> {
        loop {
            terminal.draw(|frame| self.draw_ui(frame))?;

            match rx.recv()? {
                Event::Input(event) => self.handle_key_event(event),
                Event::Tick => {
                    // app_state.on_tick();
                }
            }

            if self.app_state.should_quit() {
                return Ok(());
            }
        }
    }

    fn draw_ui(&mut self, frame: &mut Frame) {
        let contraption_layout = self.app_state.contraption_layout();

        for y in 0 .. contraption_layout.height() {
            let mut spans: Vec<Span> = vec![];
            let row = contraption_layout.get_row(y);

            for c in row.iter() {
                spans.push(Span::raw(format!("{}", c)));
            }

            let p = Paragraph::new(Line::from(spans))
                .alignment(Alignment::Left);

            let render_rect = Rect::new(
                frame.size().x,
                frame.size().y + y as u16,
                frame.size().width,
                1 
            );

            frame.render_widget(p, render_rect);
        }
    }

    fn handle_key_event(&mut self, key_event: event::KeyEvent) {
        match key_event.code {
            KeyCode::Char(c) => {
                self.app_state.on_key(c);
            }
            _ => {}
        }
    }
}
