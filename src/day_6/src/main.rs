const EXAMPLE: &str = r"Time:      7  15   30
Distance:  9  40  200";

fn main() {
    // let input = EXAMPLE;
    let input = std::fs::read_to_string("input.txt").unwrap();

    let input = pre_process_input(&input);

    println!("{}", input);

    let mut lines = input.lines();

    if let Some(line) = lines.next() {
        let race_durations: Vec<_> = line[ 9 .. ]
            .split_whitespace()
            .map(|t| t.parse::<i64>().unwrap())
            .collect();

        if let Some(line) = lines.next() {
            let distances: Vec<_> = line[ 9 .. ]
                .split_whitespace()
                .map(|d| d.parse::<i64>().unwrap())
                .collect();

            println!("{:?}", race_durations);
            println!("{:?}", distances);

            let mut races: Vec<Race> = vec![];

            for i in 0 .. race_durations.len() {
                races.push(Race {
                    duration: race_durations[i],
                    distance: distances[i],
                })
            }

            let ways_to_win: Vec<_> = races
                .iter()
                .map(|race| race.ways_to_win())
                .collect();

            println!("{:?}", ways_to_win);

            let mut ways_to_win_iter = ways_to_win.iter();
            let mut overall = 0;

            if let Some(w) = ways_to_win_iter.next() {
                overall = *w;

                while let Some(w) = ways_to_win_iter.next() {
                    overall *= *w;
                }
            }

            println!("{}", overall);
        }
    }
}

fn pre_process_input(input: &str) -> String {
    let mut processed_input = String::new();

    for line in input.lines() {
        let parts: Vec<_> = line[ 9 .. ]
            .split_whitespace()
            .collect();

        processed_input.push_str(&format!("{} {}\n", &line[ .. 9], parts.join("")));
    }

    processed_input
}

struct Race {
    duration: i64,
    distance: i64,
}

impl Race {
    pub fn ways_to_win(&self) -> i64 {
        let mut ways_to_win = 0;

        for hold_time in 0 .. self.duration {
            if self.calculate_distance(hold_time) > self.distance {
                ways_to_win += 1;
            }
        }

        ways_to_win
    }

    fn calculate_distance(&self, hold_time: i64) -> i64 {
        if hold_time == 0 || hold_time == self.duration {
            0
        } else {
            let speed = hold_time;
            speed * (self.duration - hold_time)
        }
    }
}
