use aoc_core::text_map::TextMap;

fn main() {
    let raw_schematic = std::fs::read_to_string("input.txt").unwrap();
    let engine_schematic = EngineSchematic::new(&raw_schematic);
    let all_part_numbers = engine_schematic.find_part_numbers();
    let sum_of_all_part_numbers = all_part_numbers.iter().sum::<u64>();

    println!();
    println!("The sum of all part numbers is: {}", sum_of_all_part_numbers);

    let gears = engine_schematic.find_gears();
    let sum_of_all_gears = gears.iter().sum::<u64>();

    println!();
    println!("The sum of all gears is: {}", sum_of_all_gears);
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Number {
    x: usize,
    y: usize,
    len: usize,
    number: u64,
}

impl Number {
    pub fn from_str(x: usize, y: usize, number_str: &str) -> Self {
        let len = number_str.len();
        let number = number_str.parse::<u64>().unwrap();

        Self {
            x,
            y,
            len,
            number
        }
    }

    /// Returns `true` if the number intersects with the coordinates.
    ///
    /// # Argumenst
    ///
    /// - `x` - the x-coordinate to be checked
    /// - `y` - the y-coordinate to be checked
    pub fn intersects(&self, x: usize, y: usize) -> bool {
        if self.y == y {
            x >= self.x && x < self.x + self.len
        } else {
            false
        }
    }
}

struct EngineSchematic {
    text_map: TextMap,
}

impl EngineSchematic {
    pub fn new(raw_schematic: &str) -> Self {
        let text_map = TextMap::from_str(raw_schematic);

        Self {
            text_map,
        }
    }

    fn extract_numbers(&self) -> Vec<Number> {
        let mut numbers = vec![];

        for y in 0 .. self.text_map.height() {
            let mut x = 0;

            while x < self.text_map.width() {
                if self.text_map.char_at(x, y).is_digit(10) {
                    let mut end_x_number = x;

                    while end_x_number < self.text_map.width() {
                        if !self.text_map.char_at(end_x_number, y).is_digit(10) {
                            break;
                        }

                        end_x_number += 1;
                    }

                    let start_index = self.text_map.index_of(x, y);
                    let end_index = self.text_map.index_of(end_x_number, y);

                    // extract the number
                    let number_sub_string =
                        self.text_map.sub_string_by_index(start_index, end_index);

                    numbers.push(Number::from_str(x, y, &number_sub_string));

                    x += end_index - start_index;
                } else {
                    x += 1;
                }
            }
        }

        numbers
    }

    fn find_part_numbers(&self) -> Vec<u64> {
        let mut part_numbers = vec![];

        let numbers = self.extract_numbers();

        for number in numbers.iter() {
            print!("Checking: {} (x: {}, y: {}) for adjacent symbols ",
                number.number, number.x, number.y);

            // check the char to the left
            if number.x > 0
                && !self.is_dot(number.x - 1, number.y)
            {
                println!("-> is a part number");

                part_numbers.push(number.number);
                continue;
            }

            // check the char to the right
            if number.x + number.len < self.text_map.width()
                && !self.is_dot(number.x + number.len, number.y)
            {
                println!("-> is a part number");

                part_numbers.push(number.number);
                continue;
            }

            // check the line above the number
            if number.y > 0 {
                let start_x = if number.x > 0 {
                    number.x - 1
                } else {
                    0
                };

                let end_x = if number.x + number.len < self.text_map.width() {
                    number.x + number.len
                } else {
                    number.x + number.len - 1
                };

                let mut is_part_number = false;

                for x in start_x .. end_x + 1 {
                    if !self.is_dot(x, number.y - 1) {
                        is_part_number = true;
                        break;
                    }
                }

                if is_part_number {
                    println!("-> is a part number");

                    part_numbers.push(number.number);
                    continue;
                }
            }

            // check the line below the number
            if number.y < self.text_map.height() - 1 {
                let start_x = if number.x > 0 {
                    number.x - 1
                } else {
                    0
                };

                let end_x = if number.x + number.len < self.text_map.width() {
                    number.x + number.len
                } else {
                    number.x + number.len - 1
                };

                let mut is_part_number = false;

                for x in start_x .. end_x + 1 {
                    if !self.is_dot(x, number.y + 1) {
                        is_part_number = true;
                        break;
                    }
                }

                if is_part_number {
                    println!("-> is a part number");

                    part_numbers.push(number.number);
                    continue;
                }
            }

            println!("-> is NOT a part number");
        }

        part_numbers
    }

    fn is_dot(&self, x: usize, y: usize) -> bool {
        self.text_map.char_at(x, y) == '.'
    }

    fn find_gears(&self) -> Vec<u64> {
        let mut gears: Vec<u64> = vec![];

        let numbers = self.extract_numbers();

        // everything that has a star could be possibly a gear
        let possible_gear_indizes: Vec<usize> = self.text_map.iter()
            .enumerate().filter(|(_, c)| **c == '*').map(|(index,_)| index)
            .collect();

        for gear_index in possible_gear_indizes.iter() {
            let (x, y) = self.text_map.pos_of(*gear_index);

            let neighbors = self.text_map.neighbors(x, y);
            let possible_number_indizes: Vec<usize> = neighbors.iter()
                .enumerate().filter(|(_, c)| if let Some(d) = c { d.is_digit(10) } else { false })
                .map(|(index, _)| index)
                .collect();

            let mut gear_numbers: Vec<Number> = vec![];

            for number_index in possible_number_indizes {
                let xn = x - 1 + number_index % 3;
                let yn = y - 1 + number_index / 3;

                if let Some(number) = self.find_number(&numbers, xn, yn) {
                    if !gear_numbers.contains(&number) {
                        gear_numbers.push(number);
                    }
                }
            }

            if gear_numbers.len() == 2 {
                gears.push(gear_numbers[0].number * gear_numbers[1].number);
            }
        }

        gears
    }

    fn find_number(&self, part_numbers: &[Number], x: usize, y: usize) -> Option<Number> {
        part_numbers.iter().find(|number| number.intersects(x, y)).cloned()
    }
}

#[cfg(test)]
mod tests {
    use crate::{EngineSchematic, Number};

    const EXAMPLE_ENGINE_SCHEMATIC: &str = r"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    #[test]
    fn engine_schematic_extract_numbers_should_extract_10_numbers() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);

        assert_eq!(10, engine_schematic.extract_numbers().len());
    }

    #[test]
    fn engine_schematic_extract_numbers_should_extract_first_number_as_467() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);

        assert_eq!(467, engine_schematic.extract_numbers().get(0).unwrap().number);
    }

    #[test]
    fn engine_schematic_extract_numbers_should_extract_first_number_coordinates() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);

        assert_eq!(0, engine_schematic.extract_numbers().get(0).unwrap().x);
        assert_eq!(0, engine_schematic.extract_numbers().get(0).unwrap().y);
    }

    #[test]
    fn engine_schematic_extract_numbers_should_extract_first_number_len() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);

        assert_eq!(3, engine_schematic.extract_numbers().get(0).unwrap().len);
    }

    #[test]
    fn engine_schematic_extract_numbers_should_extract_all_numbers() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);
        let all_numbers: Vec<u64> =
            engine_schematic.extract_numbers().iter().map(|n| n.number).collect();

        assert_eq!(
            vec![
                467,
                114,
                35,
                633,
                617,
                58,
                592,
                755,
                664,
                598,
            ],
            all_numbers
        );
    }

    #[test]
    fn engine_schematic_find_part_numbers_should_return_8_part_numbers() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);

        assert_eq!(8, engine_schematic.find_part_numbers().len());
    }

    #[test]
    fn engine_schematic_find_part_numbers_should_return_all_part_numbers() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);
        let all_part_numbers = engine_schematic.find_part_numbers();

        assert_eq!(
            vec![
                467,
                35,
                633,
                617,
                592,
                755,
                664,
                598,
            ],
            all_part_numbers
        );
    }

    #[test]
    fn engine_schematic_find_gears_should_return_all_gears() {
        let engine_schematic = EngineSchematic::new(EXAMPLE_ENGINE_SCHEMATIC);
        let gears = engine_schematic.find_gears();

        assert_eq!(
            vec![
                16345,
                451490,
            ],
            gears
        );
    }

    const EXTRACT_NUMBERS_EXAMPLE: &str = r"467.17.114
...*......
..35..633.
......#...
617*......
.....+.58*
..592.....
.......755
...$......
664.23.598";

    #[test]
    fn engine_schematic_extract_numbers_should_extract_12_numbers() {
        let engine_schematic = EngineSchematic::new(EXTRACT_NUMBERS_EXAMPLE);

        assert_eq!(12, engine_schematic.extract_numbers().len());
    }

    #[test]
    fn engine_schematic_extract_numbers_should_extract_all_numbers_including_edge_cases() {
        let engine_schematic = EngineSchematic::new(EXTRACT_NUMBERS_EXAMPLE);
        let all_numbers: Vec<u64> =
            engine_schematic.extract_numbers().iter().map(|n| n.number).collect();

        assert_eq!(
            vec![
                467,
                17,
                114,
                35,
                633,
                617,
                58,
                592,
                755,
                664,
                23,
                598,
            ],
            all_numbers
        );
    }

    #[test]
    fn number_intersects_0_0_number_755_should_return_true_for_0_0() {
        let number = Number::from_str(0, 0, "755");
        assert!(number.intersects(0,0));
    }

    #[test]
    fn number_intersects_0_0_number_755_should_return_true_for_1_0() {
        let number = Number::from_str(0, 0, "755");
        assert!(number.intersects(1,0));
    }

    #[test]
    fn number_intersects_0_0_number_755_should_return_true_for_2_0() {
        let number = Number::from_str(0, 0, "755");
        assert!(number.intersects(2,0));
    }

    #[test]
    fn number_intersects_0_0_number_755_should_return_false_for_3_0() {
        let number = Number::from_str(0, 0, "755");
        assert!(!number.intersects(3,0));
    }

    #[test]
    fn number_intersects_3_0_number_755_should_return_false_for_2_0() {
        let number = Number::from_str(3, 0, "755");
        assert!(!number.intersects(2,0));
    }

    #[test]
    fn number_intersects_3_0_number_755_should_return_false_for_3_1() {
        let number = Number::from_str(3, 0, "755");
        assert!(!number.intersects(3,1));
    }
}

