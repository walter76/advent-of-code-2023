use std::fmt::Display;

use once_cell::sync::Lazy;
use regex::Regex;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct GameSet {
    blue_cubes: u64,
    red_cubes: u64,
    green_cubes: u64,
}

impl GameSet {
    pub fn new(blue_cubes: u64, red_cubes: u64, green_cubes: u64) -> Self {
        Self {
            blue_cubes,
            red_cubes,
            green_cubes,
        }
    }

    pub fn from_str(game_set_text: &str) -> Self {
        Self::from(game_set_text.to_string())
    }

    pub fn blue(&self) -> u64 {
        self.blue_cubes
    }

    pub fn red(&self) -> u64 {
        self.red_cubes
    }

    pub fn green(&self) -> u64 {
        self.green_cubes
    }

    pub fn is_possible(
        &self, max_blue_cubes: u64, max_red_cubes: u64, max_green_cubes: u64
    ) -> bool {
        self.blue_cubes <= max_blue_cubes
            && self.red_cubes <= max_red_cubes
            && self.green_cubes <= max_green_cubes
    }

    pub fn power(&self) -> u64 {
        self.blue_cubes * self.red_cubes * self.green_cubes
    }
}

static BLUE_CUBES_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"(\d+)\sblue").unwrap()
});

static RED_CUBES_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"(\d+)\sred").unwrap()
});

static GREEN_CUBES_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"(\d+)\sgreen").unwrap()
});

impl From<String> for GameSet {
    fn from(s: String) -> Self {
        let mut blue_cubes: u64 = 0;

        for (_, [cubes]) in BLUE_CUBES_REGEX.captures_iter(&s).map(|c| c.extract()) {
            blue_cubes += cubes.parse::<u64>().unwrap();
        }

        let mut red_cubes: u64 = 0;

        for (_, [cubes]) in RED_CUBES_REGEX.captures_iter(&s).map(|c| c.extract()) {
            red_cubes += cubes.parse::<u64>().unwrap();
        }

        let mut green_cubes: u64 = 0;

        for (_, [cubes]) in GREEN_CUBES_REGEX.captures_iter(&s).map(|c| c.extract()) {
            green_cubes += cubes.parse::<u64>().unwrap();
        }

        Self::new(blue_cubes, red_cubes, green_cubes)
    }
}

impl Display for GameSet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "blue: {}, red: {}, green: {}", self.blue_cubes, self.red_cubes, self.green_cubes)
    }
}

#[cfg(test)]
mod tests {
    use crate::game_set::GameSet;

    static GAME_SET_1: &str = r"8 green, 6 blue, 20 red";
    static GAME_SET_2: &str = r"5 green, 1 red";
    static GAME_SET_3: &str = r"13 green, 20 red, 6 blue";

    #[test]
    fn from_game_set_1_should_8_green_6_blue_20_red() {
        let game_set = GameSet::from(GAME_SET_1.to_string());

        assert_eq!(8, game_set.green());
        assert_eq!(6, game_set.blue());
        assert_eq!(20, game_set.red());
    }

    #[test]
    fn from_game_set_2_should_5_green_0_blue_1_red() {
        let game_set = GameSet::from(GAME_SET_2.to_string());

        assert_eq!(5, game_set.green());
        assert_eq!(0, game_set.blue());
        assert_eq!(1, game_set.red());
    }

    #[test]
    fn is_possible_should_return_false_for_game_set_1() {
        let game_set = GameSet::from(GAME_SET_1.to_string());

        assert!(!game_set.is_possible(14, 12, 13));
    }

    #[test]
    fn is_possible_should_return_true_for_game_set_2() {
        let game_set = GameSet::from(GAME_SET_2.to_string());

        assert!(game_set.is_possible(14, 12, 13));
    }

    #[test]
    fn power_for_game_set_3_should_be_1560() {
        let game_set = GameSet::from_str(GAME_SET_3);

        assert_eq!(1560, game_set.power());
    }
}

