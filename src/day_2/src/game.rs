use std::slice::Iter;

use once_cell::sync::Lazy;
use regex::Regex;

use crate::game_set::GameSet;

#[derive(Debug, Default, Eq, PartialEq)]
pub struct Game {
    id: u64,
    game_sets: Vec<GameSet>,
}

impl Game {
    pub fn new(id: u64, game_sets: Vec<GameSet>) -> Self {
        Self {
            id,
            game_sets,
        }
    }

    pub fn from_str(game_text: &str) -> Self {
        Game::from(game_text.to_string())
    }

    pub fn id(&self) -> u64 {
        self.id
    }

    pub fn iter(&self) -> Iter<GameSet> {
        self.game_sets.iter()
    }

    pub fn is_possible(
        &self, max_blue_cubes: u64, max_red_cubes: u64, max_green_cubes: u64
    ) -> bool {
        self.game_sets.iter()
            .all(|game_set| game_set.is_possible(max_blue_cubes, max_red_cubes, max_green_cubes))
    }

    pub fn minimum_number_of_cubes(&self) -> GameSet {
        let mut min_blue_cubes = 0;
        let mut min_red_cubes = 0;
        let mut min_green_cubes = 0;

        for game_set in self.iter() {
            if game_set.blue() > min_blue_cubes {
                min_blue_cubes = game_set.blue();
            }

            if game_set.red() > min_red_cubes {
                min_red_cubes = game_set.red();
            }

            if game_set.green() > min_green_cubes {
                min_green_cubes = game_set.green();
            }
        }

        GameSet::new(min_blue_cubes, min_red_cubes, min_green_cubes)
    }
}

static GAME_ID_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"^Game\s(\d+):.*$").unwrap()
});

impl From<String> for Game {
    fn from(s: String) -> Self {
        let mut id: u64 = 0;

        if let Some(captures) = GAME_ID_REGEX.captures(&s) {
            if let Some(id_capture) = captures.get(1) {
                id = id_capture.as_str().parse::<u64>().unwrap();
            }
        }

        let mut game_sets: Vec<GameSet> = vec![];

        if let Some(pos) = s.find(':') {
            for game_set_string in s[ pos + 1 .. ].split(';') {
                game_sets.push(GameSet::from_str(game_set_string.trim()));
            }
        }

        Self::new(id, game_sets)
    }
}

#[cfg(test)]
mod tests {
    use crate::{Game, game_set::GameSet};

    static GAME_3: &str =
        r"Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red";

    #[test]
    fn from_string_should_return_id() {
        let game = Game::from(GAME_3.to_string());

        assert_eq!(3, game.id);
    }
    
    #[test]
    fn from_string_should_return_blue_cubes() {
        let game = Game::from(GAME_3.to_string());
        let game_set = &game.game_sets[0];

        assert_eq!(6, game_set.blue());
    }
    
    #[test]
    fn from_string_should_return_red_cubes() {
        let game = Game::from(GAME_3.to_string());
        let game_set = &game.game_sets[0];

        assert_eq!(20, game_set.red());
    }
    
    #[test]
    fn from_string_should_return_green_cubes() {
        let game = Game::from(GAME_3.to_string());
        let game_set = &game.game_sets[0];

        assert_eq!(8, game_set.green());
    }

    #[test]
    fn from_string_should_parse_game_3() {
        let game = Game::from(GAME_3.to_string());
        
        assert_eq!(
            Game::new(
                3,
                vec![
                    GameSet::new(6, 20, 8),
                    GameSet::new(5, 4, 13),
                    GameSet::new(0, 1, 5),
                ],
            ), game);
    }

    #[test]
    fn minimum_number_of_cubes_should_return_20_red_13_green_6_blue_for_game_3() {
        let game = Game::from_str(GAME_3);

        assert_eq!(GameSet::new(6, 20, 13), game.minimum_number_of_cubes());
    }
}

