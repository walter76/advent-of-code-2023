use game::Game;

mod game;
mod game_set;

fn main() {
    let game_records_input = std::fs::read_to_string("input.txt").unwrap();
    let sum_of_possible_games_ids = sum_of_possible_games_ids(&game_records_input);

    println!("The sum is: {}", sum_of_possible_games_ids);

    let total_sum_of_powers = total_sum_of_powers(&game_records_input);

    println!("The total sum of powers is: {}", total_sum_of_powers);
}

fn sum_of_possible_games_ids(game_records_input: &str) -> u64 {
    let game_records = parse_game_records(game_records_input);
    let possible_games_ids = ids_of_possible_games(14, 12, 13, &game_records);

    possible_games_ids.iter().sum()
}

fn parse_game_records(game_records_input: &str) -> Vec<Game> {
    let mut game_records = vec![];

    for line in game_records_input.lines() {
        game_records.push(Game::from_str(line));
    }

    game_records
}

fn ids_of_possible_games(
    max_blue_cubes: u64, max_red_cubes: u64, max_green_cubes: u64, game_records: &[Game]
) -> Vec<u64> {
    game_records.iter()
        .filter(|game| game.is_possible(max_blue_cubes, max_red_cubes, max_green_cubes))
        .map(|game| game.id()).collect()
}

fn total_sum_of_powers(game_records_input: &str) -> u64 {
    let game_records = parse_game_records(game_records_input);

    game_records.iter().map(|game| game.minimum_number_of_cubes().power()).sum()
}

#[cfg(test)]
mod tests {
    use crate::{
        Game, parse_game_records, ids_of_possible_games, sum_of_possible_games_ids,
        game_set::GameSet, total_sum_of_powers};

    static EXAMPLE_GAME_RECORDS_INPUT: &str = r"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn parse_game_records_should_parse_example() {
        let game_records = parse_game_records(EXAMPLE_GAME_RECORDS_INPUT);

        assert_eq!(vec![
            Game::new(1,
                vec![
                    GameSet::new(3, 4, 0),
                    GameSet::new(6, 1, 2),
                    GameSet::new(0, 0, 2),
                ]
            ),
            Game::new(2,
                vec![
                    GameSet::new(1, 0, 2),
                    GameSet::new(4, 1, 3),
                    GameSet::new(1, 0, 1),
                ]
            ),
            Game::new(3,
                vec![
                    GameSet::new(6, 20, 8),
                    GameSet::new(5, 4, 13),
                    GameSet::new(0, 1, 5),
                ]
            ),
            Game::new(4,
                vec![
                    GameSet::new(6, 3, 1),
                    GameSet::new(0, 6, 3),
                    GameSet::new(15, 14, 3),
                ]
            ),
            Game::new(5,
                vec![
                    GameSet::new(1, 6, 3),
                    GameSet::new(2, 1, 2),
                ]
            ),
        ], game_records);
    }

    #[test]
    fn ids_of_possible_games_should_return_1_2_5_for_example() {
        let game_records = parse_game_records(EXAMPLE_GAME_RECORDS_INPUT);
        let possible_games_ids = ids_of_possible_games(14, 12, 13, &game_records);

        assert_eq!(vec![1, 2, 5], possible_games_ids);
    }

    #[test]
    fn sum_of_possible_games_should_return_8_for_example() {
        assert_eq!(8, sum_of_possible_games_ids(EXAMPLE_GAME_RECORDS_INPUT));
    }

    #[test]
    fn total_sum_of_powers_should_return_2286_for_example() {
        assert_eq!(2286, total_sum_of_powers(EXAMPLE_GAME_RECORDS_INPUT));
    }
}

