use aoc_core::text_map::TextMap;

const INPUT: &str = r"R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)";

fn main() {
    let input = std::fs::read_to_string("input.txt").unwrap();

    dig(&input);
}

const DIGGING_START_X: usize = 500;
const DIGGING_START_Y: usize = 500;
const LAGOON_WIDTH: usize = 10000;
const LAGOON_HEIGHT: usize = 10000;

fn dig(input: &str) {
    let dig_plan = parse_dig_plan(input);
    let mut lagoon = TextMap::new(LAGOON_WIDTH, LAGOON_HEIGHT, vec![]);
    lagoon.fill('.');

    let mut x: usize = DIGGING_START_X;
    let mut y: usize = DIGGING_START_Y;

    for op in dig_plan.iter() {
        match op.command {
            DiggingCommand::Up(meters) => for _ in 0 .. meters {
                lagoon.set_char_at(x, y, '#');
                y -= 1;
            }

            DiggingCommand::Right(meters) => for _ in 0 .. meters {
                lagoon.set_char_at(x, y, '#');
                x += 1;
            }

            DiggingCommand::Down(meters) => for _ in 0 .. meters {
                lagoon.set_char_at(x, y, '#');
                y += 1;
            }

            DiggingCommand::Left(meters) => for _ in 0 .. meters {
                lagoon.set_char_at(x, y, '#');
                x -= 1;
            }
        }
    }

    // println!("{}", lagoon);

    for y_offset in 0 .. 20 {
        let y = DIGGING_START_Y - 10 + y_offset;

        for x_offset in 0 .. 20 {
            let x = DIGGING_START_X - 10 + x_offset;
            print!("{}", lagoon.char_at(x, y));
        }

        println!();
    }

    flood_fill(&mut lagoon, DIGGING_START_X + 1, DIGGING_START_Y + 1);

    // println!("{}", lagoon);

    for y_offset in 0 .. 20 {
        let y = DIGGING_START_Y - 10 + y_offset;

        for x_offset in 0 .. 20 {
            let x = DIGGING_START_X - 10 + x_offset;
            print!("{}", lagoon.char_at(x, y));
        }

        println!();
    }

    let tiles_digged = lagoon.iter().filter(|c| **c == '#').count();
    println!("{}", tiles_digged);
}

fn parse_dig_plan(input: &str) -> Vec<DiggingOperation> {
    let mut dig_plan: Vec<DiggingOperation> = vec![];

    for line in input.lines() {
        dig_plan.push(DiggingOperation::from_str(line));
    }

    dig_plan
}

fn flood_fill(lagoon: &mut TextMap, x: usize, y: usize) {
    // already filled -> return
    if lagoon.char_at(x, y) == '#' {
        return;
    }

    lagoon.set_char_at(x, y, '#');

    flood_fill(lagoon, x, y - 1);   // go up
    flood_fill(lagoon, x + 1, y);   // go right
    flood_fill(lagoon, x, y + 1);   // go down
    flood_fill(lagoon, x - 1, y);   // go left
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum DiggingCommand {
    Up(usize),
    Right(usize),
    Down(usize),
    Left(usize),
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {
    pub fn from_str(rgb_text: &str) -> Self {
        let red = u8::from_str_radix(&rgb_text[0 .. 2], 16).unwrap();
        let green = u8::from_str_radix(&rgb_text[2 .. 4], 16).unwrap();
        let blue = u8::from_str_radix(&rgb_text[4 .. ], 16).unwrap();

        Self {
            red,
            green,
            blue,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
struct DiggingOperation {
    command: DiggingCommand,
    color: Color,
}

impl DiggingOperation {
    pub fn from_str(op_text: &str) -> Self {
        let chunks: Vec<String> = op_text.split_whitespace().map(|s| s.to_string()).collect();

        let meters = chunks[1].parse::<usize>().unwrap();

        let command = match chunks[0].as_str() {
            "U" => DiggingCommand::Up(meters),
            "R" => DiggingCommand::Right(meters),
            "D" => DiggingCommand::Down(meters),
            "L" => DiggingCommand::Left(meters),
            _ => unimplemented!(),
        };

        let color = Color::from_str(&chunks[2][2 .. 8]);

        Self {
            command,
            color,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Color, DiggingOperation, DiggingCommand};

    const RGB_EXAMPLE: &str = "70c710";

    #[test]
    fn color_from_str_should_parse_70c710() {
        let color = Color::from_str(RGB_EXAMPLE);

        assert_eq!(Color { red: 0x70, green: 0xc7, blue: 0x10 }, color);
    }

    const DIGGING_OPERATION_UP_EXAMPLE: &str = "U 2 (#caa173)";

    #[test]
    fn digging_operation_from_str_should_parse_digging_up_example() {
        let digging_operation = DiggingOperation::from_str(DIGGING_OPERATION_UP_EXAMPLE);

        assert_eq!(
            DiggingOperation {
                command: DiggingCommand::Up(2),
                color: Color { red: 0xca, green: 0xa1, blue: 0x73 },
            },
            digging_operation
        );
    }

    const DIGGING_OPERATION_RIGHT_EXAMPLE: &str = "R 6 (#70c710)";

    #[test]
    fn digging_operation_from_str_should_parse_digging_right_example() {
        let digging_operation = DiggingOperation::from_str(DIGGING_OPERATION_RIGHT_EXAMPLE);

        assert_eq!(
            DiggingOperation {
                command: DiggingCommand::Right(6),
                color: Color { red: 0x70, green: 0xc7, blue: 0x10 },
            },
            digging_operation
        );
    }

    const DIGGING_OPERATION_DOWN_EXAMPLE: &str = "D 5 (#0dc571)";

    #[test]
    fn digging_operation_from_str_should_parse_digging_down_example() {
        let digging_operation = DiggingOperation::from_str(DIGGING_OPERATION_DOWN_EXAMPLE);

        assert_eq!(
            DiggingOperation {
                command: DiggingCommand::Down(5),
                color: Color { red: 0x0d, green: 0xc5, blue: 0x71 },
            },
            digging_operation
        );
    }

    const DIGGING_OPERATION_LEFT_EXAMPLE: &str = "L 2 (#5713f0)";

    #[test]
    fn digging_operation_from_str_should_parse_digging_left_example() {
        let digging_operation = DiggingOperation::from_str(DIGGING_OPERATION_LEFT_EXAMPLE);

        assert_eq!(
            DiggingOperation {
                command: DiggingCommand::Left(2),
                color: Color { red: 0x57, green: 0x13, blue: 0xf0 },
            },
            digging_operation
        );
    }
}

