use std::{collections::HashMap, time::Instant};

use crate::scratch_card::ScratchCard;

mod scratch_card;

fn main() {
    let pile_of_cards_input = std::fs::read_to_string("input.txt").unwrap();

    let took_time = Instant::now();

    let sum_of_points = sum_of_winning_cards(&pile_of_cards_input);

    let elapsed = took_time.elapsed();

    println!("Sum of points is: {} (took: {:.2?})", sum_of_points, elapsed);
 
    let took_time = Instant::now();

    let total = total_scratch_cards(&pile_of_cards_input);

    let elapsed = took_time.elapsed();

    // let total = total_scratch_cards_brute_force(&pile_of_cards_input);
    println!("Total is: {} (took: {:.2?})", total, elapsed);
}

fn sum_of_winning_cards_verbose(pile_of_cards_input: &str) -> u64 {
    let mut sum = 0;

    let lines: Vec<String> = pile_of_cards_input.lines().map(|s| s.to_string()).collect();

    for line in lines[0 .. 10].iter() {
        println!("{}", line);

        let card = ScratchCard::from(line.clone());
        let points = card.points();
        println!("{}", points);
        println!();

        sum += points;
    }

    sum
}

fn sum_of_winning_cards(pile_of_cards_input: &str) -> u64 {
    parse_input(pile_of_cards_input)
        .iter()
        .map(|card| card.points() as u64)
        .sum()
}

/// Brute force of part 2. This takes loooooong.
fn total_scratch_cards_brute_force(pile_of_cards_input: &str) -> usize {
    let mut final_pile_of_cards: Vec<u16> = vec![];
    let mut numbers_won_count_per_card: HashMap<u16, usize> = HashMap::new();

    for scratch_card in parse_input(pile_of_cards_input) {
        final_pile_of_cards.push(scratch_card.id());
        numbers_won_count_per_card.insert(scratch_card.id(), scratch_card.numbers_won().len());
    }

    let mut index_to_analyze: usize = 0;

    loop {
        let current_scratch_card_id = *final_pile_of_cards.get(index_to_analyze).unwrap();
        let numbers_won_count = *numbers_won_count_per_card.get(&current_scratch_card_id).unwrap() as u16;

        for id in current_scratch_card_id + 1 .. current_scratch_card_id + 1 + numbers_won_count {
            // insert it into the final_pile_of_cards at the right position
            let index_to_insert = final_pile_of_cards.iter().position(|card_id| id == *card_id).unwrap();
            final_pile_of_cards.insert(index_to_insert, id);
        }

        if index_to_analyze < final_pile_of_cards.len() - 1 {
            println!("scratch card id: {}, index: {}, len: {}",
                current_scratch_card_id, index_to_analyze, final_pile_of_cards.len());

            index_to_analyze += 1;
        } else {
            break;
        }
    }

    final_pile_of_cards.len()
}

fn total_scratch_cards(pile_of_cards_input: &str) -> u32 {
    let pile_of_cards = parse_input(pile_of_cards_input);
    let mut quantity = vec![1u32; pile_of_cards.len()];

    for (idx, card) in pile_of_cards.iter().enumerate() {
        let count = quantity[idx];

        if let Some(quantities) = quantity.get_mut(idx + 1 .. idx + 1 + card.numbers_won().len()) {
            for quantity in quantities {
                *quantity += count;
            }
        }
    }

    quantity.iter().sum()
}

fn parse_input(pile_of_cards_input: &str) -> Vec<ScratchCard> {
    pile_of_cards_input.lines()
        .into_iter()
        .map(|card_text| ScratchCard::from_str(card_text))
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::{sum_of_winning_cards, scratch_card::ScratchCard, total_scratch_cards};

    const PILE_OF_CARDS: &str = r"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

    #[test]
    fn pile_of_cards_should_return_6_cards() {
        let pile_of_cards: Vec<_> = PILE_OF_CARDS.lines()
            .into_iter()
            .map(|card_text| ScratchCard::from_str(card_text))
            .collect();

        let card_ids: Vec<_> = pile_of_cards
            .iter()
            .map(|card| card.id())
            .collect();

        assert_eq!(
            vec![ 1, 2, 3, 4, 5, 6, ],
            card_ids
        );
    }

    #[test]
    fn sum_of_winning_cards_should_be_13_for_pile_of_cards() {
        let sum_of_points = sum_of_winning_cards(PILE_OF_CARDS);

        assert_eq!(13, sum_of_points);
    }

    #[test]
    fn total_scratch_cards_should_be_30_for_pile_of_cards() {
        assert_eq!(30, total_scratch_cards(PILE_OF_CARDS));
    }
}

