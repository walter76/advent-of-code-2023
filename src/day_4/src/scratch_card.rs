use once_cell::sync::Lazy;
use regex::Regex;

#[derive(Default, Debug, Eq, PartialEq, Clone)]
pub struct ScratchCard {
    id: u16,
    winning_numbers: Vec<u8>,
    having_numbers: Vec<u8>,
}

impl ScratchCard {
    pub fn from_str(card_as_text: &str) -> Self {
        ScratchCard::from(card_as_text.to_string())
    }

    pub fn id(&self) -> u16 {
        self.id
    }

    pub fn points(&self) -> u64 {
        let numbers_won_len = self.numbers_won().len();

        if numbers_won_len > 0 {
            2_u64.pow(numbers_won_len as u32 - 1)
        } else {
            0
        }
    }

    pub fn numbers_won(&self) -> Vec<u8> {
        let numbers_won: Vec<_> = self.winning_numbers
            .clone()
            .into_iter()
            .filter(|n| self.having_numbers.contains(n))
            .map(|n| n)
            .collect();

        numbers_won
    }
}

static SCRATCH_CARD_REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"^Card\s+(\d+):\s(.*)\s\|\s(.*)$").unwrap()
});

impl From<String> for ScratchCard {
    fn from(card_as_text: String) -> Self {
        let mut scratch_card = Self::default();

        if let Some(captures) = SCRATCH_CARD_REGEX.captures(&card_as_text) {
            if let Some(card_id_capture) = captures.get(1) {
                scratch_card.id = card_id_capture.as_str().parse::<u16>().unwrap();
            }

            if let Some(winning_numbers_capture) = captures.get(2) {
                scratch_card.winning_numbers = winning_numbers_capture
                    .as_str()
                    .split_whitespace()
                    .map(|n| n.parse::<u8>().unwrap())
                    .collect();
            }

            if let Some(having_numbers_capture) = captures.get(3) {
                scratch_card.having_numbers = having_numbers_capture
                    .as_str()
                    .split_whitespace()
                    .map(|n| n.parse::<u8>().unwrap())
                    .collect();
            }
        }

        scratch_card
    }
}

#[cfg(test)]
mod tests {
    use crate::scratch_card::ScratchCard;

    const CARD_1_EXAMPLE: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";

    #[test]
    fn card_from_string_should_return_1_for_card_1_example() {
        let card = ScratchCard::from(CARD_1_EXAMPLE.to_string());

        assert_eq!(1, card.id);
    }

    #[test]
    fn card_from_string_should_return_winning_numbers_for_card_1_example() {
        let card = ScratchCard::from(CARD_1_EXAMPLE.to_string());

        assert_eq!(
            vec![ 41, 48, 83, 86, 17, ],
            card.winning_numbers
        );
    }

    #[test]
    fn card_from_string_should_return_having_numbers_for_card_1_example() {
        let card = ScratchCard::from(CARD_1_EXAMPLE.to_string());

        assert_eq!(
            vec![ 83, 86, 6, 31, 17, 9, 48, 53, ],
            card.having_numbers
        );
    }

    #[test]
    fn card_numbers_won_should_return_numbers_for_card_1_example() {
        let card = ScratchCard::from(CARD_1_EXAMPLE.to_string());

        assert_eq!(
            vec![ 48, 83, 86, 17, ],
            card.numbers_won()
        );
    }

    #[test]
    fn card_points_should_return_8_for_card_1_example() {
        let card = ScratchCard::from(CARD_1_EXAMPLE.to_string());
        assert_eq!(8, card.points());
    }
}

